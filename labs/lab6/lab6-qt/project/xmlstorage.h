#ifndef XMLSTORAGE_H
#define XMLSTORAGE_H


#pragma once

#include "storage.h"

using namespace std;

class XmlStorage: public FileStorage
{
   vector<Dinosaur> loadDinosaurs();
   void saveDinosaurs(const vector<Dinosaur> & dinosaurs);
   int getNewDinosaurId();

   vector<Era> loadEras();
   void saveEras(const vector<Era> &eras);
   int getNewEraSId();

 public:
   explicit XmlStorage(const string & dir_name_ = "") : FileStorage(dir_name_) {}
};
#endif // XMLSTORAGE_H
