#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include "xmlstorage.h"
#include "cui.h"
#include "csvstorage.h"
#include <optional>
#include <vector>
using namespace std;

int main()
{    cout<<"choose xml or csv"<<endl;
     string m;
     cin>>m;
     if (m=="csv")
     {
       CsvStorage csv_storage("../../data/csv/");
       FileStorage * storage_ptr = &csv_storage;

       Cui cui(storage_ptr);

       cui.show();
     }
    else if(m=="xml")
    {
        XmlStorage xml_storage("../../data/xml/");
        FileStorage * storage_ptr = &xml_storage;
        Cui cui(storage_ptr);

        cui.show();
    }
     else
     {
         cout<<"wrong storage"<<endl;
         return 0;
     }
}
