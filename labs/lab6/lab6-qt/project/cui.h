#ifndef CUI_H
#define CUI_H


using namespace std;
#include <iostream>
#include "storage.h"
#include "dino.h"
#include <optional>
#include <vector>
#pragma once
class Cui
{
    FileStorage * const storage_;

    // students menus
    void dinosaursMainMenu();
    void dinosaurMenu(int dinosaur_id);
    void dinosaurUpdateMenu(int dinosaur_id);
    void dinosaurDeleteMenu(int dinosaur_id);
    void dinosaurCreateMenu();
    void task1(string &menu);

    void eraMainMenu();
    void eraMenu(int era_id);
    void eraUpdateMenu(int era_id);
    void eraDeleteMenu(int era_id);
    void eraCreateMenu();
    void task2(string &menu);

public:
    Cui(FileStorage * storage): storage_{storage} {}
    //
    void show();
};

#endif // CUI_H
