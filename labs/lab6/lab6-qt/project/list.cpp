#include "list.h"

using namespace std;
#include "list.h"
vector<Dinosaur> createDinoListFromTable(StringTable &csvTable)
{
    vector<Dinosaur> v;
    if(csvTable.size_columns()==1&&csvTable.size_rows()==1)
    {
        return v;
    }
    else
    {
    for (int i = 0; i < csvTable.size_rows(); i++)
    {
        double weight = stod(csvTable.at(i, 3));
        double length = stod(csvTable.at(i, 2));
        Dinosaur d{csvTable.at(i, 1),  weight, length, csvTable.at(i, 4), stoi(csvTable.at(i, 0))};
        v.push_back(d);
    }
    return v;
    }
}

vector<Era> createEraListFromTable(StringTable &csvTable)
{
    vector<Era> v;
    if(csvTable.size_columns()==1&&csvTable.size_rows()==1)
    {
        return v;
    }
    else

    {

    for (int i=0; i < csvTable.size_rows(); i++)
    {
        int from=stoi(csvTable.at(i,2));
        int to=stoi(csvTable.at(i,3));
        int id=stoi(csvTable.at(i,0));
        Era e{csvTable.at(i, 1), from, to, id};
        v.push_back(e);
    }
    return v;
    }
}

StringTable createTableFromList(vector<Dinosaur>  v, size_t cols)
{
    StringTable table{v.size(), cols};
    for (int i = 0; i < v.size(); i++)
    {
        string id=to_string(v.at(i).id);
        string name = v.at(i).name;
        string order = v.at(i).order;
        string length = to_string(v.at(i).length);
        string weigth = to_string(v.at(i).weight);
        table.add(0, i, id);
        table.add(1, i, name);
        table.add(2, i, length);
        table.add(3, i, weigth);
        table.add(4, i, order);
    }
    return table;
}

StringTable createTableFromEraList(vector<Era> v, size_t cols)
{
    StringTable table{v.size(), cols};
    for (int i = 0; i < v.size(); i++)
    {
        string id=to_string(v.at(i).id);
        string name = v.at(i).name;
        string from = to_string(v.at(i).from);
        string to = to_string(v.at(i).to);
        table.add(0, i, id);
        table.add(1, i, name);
        table.add(2, i, from);
        table.add(3, i, to);
    }
    return table;
}
