#ifndef STORAGE_H
#define STORAGE_H

#include <string>
#include <vector>
#include <fstream>

#include <optional>
#include "../libcsvlab/string_table.h"
#include "fs.h"
#include "list.h"
#include "dino.h"
#include "era.h"
#pragma once
using namespace std;

class FileStorage
{
   string  dir_name_;

protected:
   fstream dinosaurs_file_;
   fstream eras_file_;

   virtual vector<Dinosaur> loadDinosaurs()=0;
   virtual void saveDinosaurs(const vector<Dinosaur> & dinosaurs)=0;
   virtual int getNewDinosaurId()=0;

   virtual vector<Era> loadEras()=0;
   virtual void saveEras(const vector<Era> & eras)=0;
   virtual int getNewEraSId()=0;

 public:
   explicit FileStorage(const string & dir_name = "");
   virtual ~FileStorage() {}
   void setName(const string & dir_name);
   string name() const;

   bool isOpen() const;
   bool open();
   void close();

   vector<Dinosaur> getAllDinosaurs(void);
   optional<Dinosaur> getDinosaurById(int dinosaur_id);
   bool updateDinosaur(const Dinosaur &dinosaur);
   bool removeDinosaur(int dinosaur_id);
   int insertDinosaur(const Dinosaur &dinosaur);

   vector<Era> getAllEras(void);
   optional<Era> getEraById(int era_id);
   bool updateEra(const Era &era);
   bool removeEra(int era_id);
   int insertEra(const Era &era);
};

#endif // STORAGE_H
