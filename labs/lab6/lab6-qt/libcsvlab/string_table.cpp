#include "string_table.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
using namespace std;

StringTable::StringTable(size_t rows, size_t cols)
{
    if (rows > 0 && cols > 0)
    {
        cells_ = new string *[rows];
        nrows_ = rows;
        ncols_ = cols;
        if (cells_ != nullptr)
        {
            for (size_t i = 0; i < rows; i++)
            {
                cells_[i] = new string[cols];
            }
        }
        else
        {
            cerr << "memory allocating error" << endl;
        }
    }
    else
    {
        //cerr << "cannot be created" << endl;
    }
}
StringTable::~StringTable()
{
    for (size_t i = 0; i < nrows_; i++)
    {
        delete[] cells_[i];
    }
    delete[] cells_;
    nrows_ = 0;
    ncols_ = 0;
}
size_t StringTable::size_rows()
{
    return nrows_;
}
size_t StringTable::size_columns()
{
    return ncols_;
}
string &StringTable::at(int rowIndex, int colIndex)
{

    return cells_[rowIndex][colIndex];
}
void StringTable::add(int colIndex, int rowIndex, string &value)
{
    if (rowIndex >= size_rows() || colIndex >= size_columns())
    {
        cerr << "Error wrong index, maybe file is empty" << endl;
        exit(1);
    }
    else
    {
        cells_[rowIndex][colIndex] = value;
    }
}
void StringTable::print()
{
    cout << endl
         << "string_table:" << endl;
    if (nrows_ ==1 && ncols_ == 1)
    {
        //cout << "cannot be created";
    }
    else
    {
        for (size_t i = 0; i < nrows_; i++)
        {
            for (size_t j = 0; j < ncols_; j++)
            {
                cout << cells_[i][j] << " ";
            }
            cout << endl;
        }
    }
}
