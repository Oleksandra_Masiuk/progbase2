#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
using namespace std;
#pragma once
class StringTable
{
   string **cells_ = nullptr;
   size_t nrows_ = 0;
   size_t ncols_ = 0;

public:
   StringTable(size_t rows, size_t cols);
   ~StringTable();

   size_t size_rows();
   size_t size_columns();

   string & at(int rowIndex, int colIndex);
   void add(int colIndex, int rowIndex, string & value);
   void print ();
   
};