#include "cui.h"
#include "storage.h"
using namespace std;
void Cui::dinosaursMainMenu()
{
    vector<Dinosaur> dino = storage_->getAllDinosaurs();
    for (Dinosaur &d : dino)
    {
        cout << d.id << " | " << d.name << endl;
    }
}
void Cui::eraMainMenu()
{
    vector<Era> era = storage_->getAllEras();
    for (Era &e : era)
    {
        cout << e.id << " | " << e.name << endl;
    }
}
void Cui::dinosaurMenu(int dinosaur_id)
{
    optional<Dinosaur> dopt = storage_->getDinosaurById(dinosaur_id);
    if (dopt)
    {
        Dinosaur &d = dopt.value();
        cout << d.id << " | " << d.name << " | "
             << "w=" << d.weight << " | "
             << " l= " << d.length << " | "
             << "order: " << d.order << endl;
    }
    else
    {
        cout << "wrong id try again" << endl;
    }
}
void Cui::eraMenu(int era_id)
{
    optional<Era> eopt = storage_->getEraById(era_id);
    if (eopt)
    {
        Era &e = eopt.value();
        cout << e.id << " | " << e.name << " | " << e.from << " | " << e.to << endl;
    }
    else
    {
        cout << "wrong id try again" << endl;
    }
}
void Cui::dinosaurUpdateMenu(int dinosaur_id)
{
    cout << "choose name, weight, length or order" << endl;
    string menu;
    cin >> menu;
    if (menu == "name" || menu == "weight" || menu == "length" || menu == "order")
    {
        optional<Dinosaur> dopt = storage_->getDinosaurById(dinosaur_id);
        if (dopt)
        {
            Dinosaur &d = dopt.value();
            if (menu == "name")
            {
                menu = "";
                cin >> menu;
                d.name = menu;
                if (storage_->updateDinosaur(d))
                {
                    cout << "dinosaur updated" << endl;
                }
                else
                {
                    cout << "cannot update this dinosaur" << endl;
                }
            }
            if (menu == "order")
            {
                menu = "";
                cin >> menu;
                d.order = menu;
                if (storage_->updateDinosaur(d))
                {
                    cout << "dinosaur updated" << endl;
                }
                else
                {
                    cout << "cannot update this dinosaur" << endl;
                }
            }
            if (menu == "weight")
            {
                char w[20];
                cin >> w;
                if ((w, "0") != 0 && atof(w) == 0)
                {
                    cout << "wrong weight" << endl;
                }
                else
                {
                    d.weight = atof(w);
                    if (storage_->updateDinosaur(d))
                    {
                        cout << "dinosaur updated" << endl;
                    }
                    else
                    {
                        cout << "cannot update this dinosaur" << endl;
                    }
                }
            }
            if (menu == "length")
            {
                char l[20];
                cin >> l;
                if ((l, "0") != 0 && atof(l) == 0)
                {
                    cout << "wrong weight" << endl;
                }
                else
                {
                    d.length = atof(l);
                    if (storage_->updateDinosaur(d))
                    {
                        cout << "dinosaur updated" << endl;
                    }
                    else
                    {
                        cout << "cannot update this dinosaur" << endl;
                    }
                }
            }
        }
        else
        {
            cout << "wrong id, try again" << endl;
        }
    }
    else
    {
        cout << "wrong option try again";
    }
}
void Cui::eraUpdateMenu(int era_id)
{
    cout << "choose name, start or end" << endl;
    string menu;
    cin >> menu;
    if (menu == "name" || menu == "start" || menu == "end")
    {
        optional<Era> eopt = storage_->getEraById(era_id);
        if (eopt)
        {
            Era &e = eopt.value();
            if (menu == "name")
            {
                menu = "";
                cin >> menu;
                e.name = menu;
                if (storage_->updateEra(e))
                {
                    cout << "era updated" << endl;
                }
                else
                {
                    cout << "cannot update this era" << endl;
                }
            }
            if (menu == "start")
            {
                char f[20];
                cin >> f;
                if ((f, "0") != 0 && atoi(f) == 0)
                {
                    cout << "wrong time" << endl;
                }
                else
                {
                    e.from = atoi(f);
                    if (storage_->updateEra(e))
                    {
                        cout << "era updated" << endl;
                    }
                    else
                    {
                        cout << "cannot update this era" << endl;
                    }
                }
            }
            if (menu == "end")
            {
                char t[20];
                cin >> t;
                if ((t, "0") != 0 && atoi(t) == 0)
                {
                    cout << "wrong time" << endl;
                }
                else
                {
                    e.to = atoi(t);
                    if (storage_->updateEra(e))
                    {
                        cout << "era updated" << endl;
                    }
                    else
                    {
                        cout << "cannot update this era" << endl;
                    }
                }
            }
        }
        else
        {
            cout << "wrong id, try again" << endl;
        }
    }
    else
    {
        cout << "wrong option try again";
    }
}
void Cui::dinosaurDeleteMenu(int dinosaur_id)
{
    bool status = storage_->removeDinosaur(dinosaur_id);
    if (status)
    {
        cout << "removed" << endl;
    }
    else
    {
        cout << "cannot remove" << endl;
    }
}
void Cui::eraDeleteMenu(int era_id)
{
    bool status = storage_->removeEra(era_id);
    if (status)
    {
        cout << "removed" << endl;
    }
    else
    {
        cout << "cannot remove" << endl;
    }
}
void Cui::dinosaurCreateMenu()
{
    string name;
    char weight[20];
    string order;
    char length[20];
    cout << "please, enter name, weight, length and order" << endl;
    cin >> name;
    cin >> weight;
    cin >> length;
    cin >> order;
    if ((atof(weight) == 0 && (weight, "0") != 0) || (atof(length) == 0 && (length, "0") != 0))
    {
        cout << "wrong length or weigth " << endl;
    }
    else
    {
        Dinosaur d{name, stod(weight), stod(length), order, -1};
        int newId = storage_->insertDinosaur(d);
        d.id = newId;
    }
}
void Cui::eraCreateMenu()
{

    string name;
    char from[20];
    char to[20];
    cout << "enter name, start or end" << endl;
    cin >> name;
    cin >> from;
    cin >> to;
    if ((atoi(from) == 0 && (from, "0") != 0) || (atoi(to) == 0 && (to, "0") != 0))
    {
        cout << "wrong time " << endl;
    }
    else
    {
        int f, t;
        if (stoi(from) > stoi(to))
        {
            f = stoi(from);
            t = stoi(to);
        }
        else
        {
            t = stoi(from);
            f = stoi(to);
        }
        Era e{name, f, t, -1};

        int newId = storage_->insertEra(e);
        e.id = newId;
    }
}
void Cui::task1(string &menu)
{
    if (menu != "exit" && menu != "get_dino" && menu != "get_all_dino" && menu != "remove_dino" && menu != "update_dino" && menu != "add_dino")
    {
        cout << "wrong option, try again" << endl;
    }
    else if (menu == "get_dino")
    {
        char id[20];
        cout << "enter id" << endl;
        cin >> id;
        if ((atof(id) == 0 && (id, "0") != 0))
        {
            cout << "wrong id" << endl;
        }
        else
        {
            this->dinosaurMenu(atoi(id));
        }
    }
    else if (menu == "get_all_dino")
    {
        this->dinosaursMainMenu();
    }
    else if (menu == "update_dino")
    {
        char id[20];
        cout << "enter id" << endl;
        cin >> id;
        if ((atoi(id) == 0 && (id, "0") != 0))
        {
            cout << "wrong id" << endl;
        }
        else
        {
            this->dinosaurUpdateMenu(atoi(id));
        }
    }
    else if (menu == "remove_dino")
    {
        char id[20];
        cout << "enter id" << endl;
        cin >> id;
        if ((atoi(id) == 0 && (id, "0") != 0))
        {
            cout << "wrong id" << endl;
        }
        else
        {
            this->dinosaurDeleteMenu(atoi(id));
        }
    }
    else if (menu == "add_dino")
    {
        this->dinosaurCreateMenu();
    }
}
void Cui::task2(string &menu)
{
    if (menu != "exit" && menu != "get_era" && menu != "get_all_era" && menu != "remove_era" && menu != "update_era" && menu != "add_era")
    {
        cout << "wrong option, try again" << endl;
    }
    else if (menu == "get_era")
    {
        char id[20];
        cout << "enter id" << endl;
        cin >> id;
        if ((atof(id) == 0 && (id, "0") != 0))
        {
            cout << "wrong id" << endl;
        }
        else
        {
            this->eraMenu(atoi(id));
        }
    }
    else if (menu == "get_all_era")
    {
        this->eraMainMenu();
    }
    else if (menu == "update_era")
    {
        char id[20];
        cout << "enter id" << endl;
        cin >> id;
        if ((atoi(id) == 0 && (id, "0") != 0))
        {
            cout << "wrong id" << endl;
        }
        else
        {
            this->eraUpdateMenu(atoi(id));
        }
    }
    else if (menu == "remove_era")
    {
        char id[20];
        cout << "enter id" << endl;
        cin >> id;
        if ((atoi(id) == 0 && (id, "0") != 0))
        {
            cout << "wrong id" << endl;
        }
        else
        {
            this->eraDeleteMenu(atoi(id));
        }
    }
    else if (menu == "add_era")
    {
        this->eraCreateMenu();
    }
}
void Cui::show()
{
    string menu;
    while (menu != "exit")
    {
        cout << "chose dino, era or exit" << endl;
        cin >> menu;
        if (menu == "dino")
        {
            menu = "";
            cout << "enter:" << endl
                 << "get_dino or get_all_dino or update_dino or remove_dino or add_dino" << endl;
            cin >> menu;
            task1(menu);
        }
        else if (menu == "era")
        {
            menu = "";
            cout << "enter:" << endl
                 << "get_era or get_all_era or update_era or remove_era or add_era" << endl;
            cin >> menu;
            task2(menu);
        }
        else if(menu!="exit")
        {
            cout << "wrong option" << endl;
        }
    }
}
