#ifndef FS_H
#define FS_H

#include <stdio.h>
#include <stdlib.h>
#include "../libcsvlab/string_table.h"
#include "../libcsvlab/csv.h"
#include <iostream>
using namespace std;
#pragma once

StringTable readfile(fstream & fin);
void writefile(fstream & fout, string & csvtable);
#endif // FS_H
