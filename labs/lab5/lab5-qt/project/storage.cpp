#include "storage.h"
using namespace std;

FileStorage::FileStorage(const string &dir_name)
{
    dir_name_ = dir_name;
}
void FileStorage::setName(const string &dir_name)
{
    dir_name_ = dir_name;
}
string FileStorage::name() const
{
    return dir_name_;
}

bool FileStorage::isOpen() const
{
    return dinosaurs_file_.is_open();
}
bool FileStorage::open()
{
    dinosaurs_file_.open(dir_name_ + "dinosaur.csv");
    return dinosaurs_file_.is_open();
}
void FileStorage::close()
{
    dinosaurs_file_.close();
}

vector<Dinosaur> FileStorage::getAllDinosaurs(void)
{
    return this->loadDinosaurs();
}
vector<Era> FileStorage::getAllEras(void)
{
    return this->loadEras();
}
optional<Dinosaur> FileStorage::getDinosaurById(int dinosaur_id)
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id == dinosaur_id)
        {
            return dino;
        }
    }
    return nullopt;
}
optional<Era> FileStorage::getEraById(int era_id)
{
    vector<Era> eras = this->loadEras();
    for (Era &era : eras)
    {
        if (era.id == era_id)
        {
            return era;
        }
    }
}
bool FileStorage::updateDinosaur(const Dinosaur &dinosaur)
{
    int dinosaur_id = dinosaur.id;
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id == dinosaur_id)
        {
            dino = dinosaur;
            this->saveDinosaurs(dinosaurs);
            return true;
        }
    }
    return false;
}
bool FileStorage::updateEra(const Era &era)
{
    int era_id = era.id;
    vector<Era> eras = this->loadEras();
    for (Era &er : eras)
    {
        if (er.id == era_id)
        {
            er = era;
            this->saveEras(eras);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeDinosaur(int dinosaur_id)
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    auto it = dinosaurs.begin();
    auto it_end = dinosaurs.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == dinosaur_id)
        {
            dinosaurs.erase(it);
            this->saveDinosaurs(dinosaurs);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeEra(int era_id)
{
    vector<Era> eras = this->loadEras();
    auto it = eras.begin();
    auto it_end = eras.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == era_id)
        {
            eras.erase(it);
            this->saveEras(eras);
            return true;
        }
    }
    return false;
}
int FileStorage::insertDinosaur(const Dinosaur &dinosaur)
{
    int newId = this->getNewDinosaurId();
    Dinosaur copy = dinosaur;
    copy.id = newId;
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    dinosaurs.push_back(copy);
    this->saveDinosaurs(dinosaurs);
    return newId;
}
int FileStorage::insertEra(const Era &era)
{
    int newId = this->getNewEraSId();
    Era copy = era;
    copy.id = newId;
    vector<Era> eras = this->loadEras();
    eras.push_back(copy);
    this->saveEras(eras);
    return newId;
}
vector<Dinosaur> FileStorage::loadDinosaurs()
{
    dinosaurs_file_.close();
    dinosaurs_file_.open(dir_name_ + "dinosaur.csv", ios::in);
    StringTable table = readfile(dinosaurs_file_);
    vector<Dinosaur> dinosaurs = createDinoListFromTable(table);
    return dinosaurs;
}
vector<Era> FileStorage::loadEras()
{
    eras_file_.close();
    eras_file_.open(dir_name_ + "eras.csv", ios::in);
    StringTable table = readfile(eras_file_);
    vector<Era> eras=createEraListFromTable(table);
    return eras;
}
void FileStorage::saveDinosaurs(const vector<Dinosaur> &dinosaurs)
{
    dinosaurs_file_.close();
    dinosaurs_file_.open(dir_name_ + "dinosaur.csv", ios::out);
    StringTable table = createTableFromList(dinosaurs, 5);
    string csvtable = Csv_toString(table);
    writefile(dinosaurs_file_, csvtable);
}
void FileStorage::saveEras(const vector<Era> &eras)
{
    eras_file_.close();
    eras_file_.open(dir_name_ + "eras.csv", ios::out);
        StringTable table=createTableFromEraList(eras, 4);
        string csvtable = Csv_toString(table);
        writefile(eras_file_, csvtable);

}
int FileStorage::getNewDinosaurId()
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    int max = 0;
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id > max)
        {
            max = dino.id;
        }
    }
    return max + 1;
}
int FileStorage::getNewEraSId()
{
    vector<Era> eras = this->loadEras();
    int max = 0;
    for (Era &er : eras)
    {
        if (er.id > max)
        {
            max = er.id;
        }
    }
    return max+1;
}
