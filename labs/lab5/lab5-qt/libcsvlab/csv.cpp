#include "csv.h"
using namespace std;

StringTable Csv_parse(string &csvStr)
{
    csvStr.pop_back();
    size_t comma = 0;
    size_t n = 0, row = 0, col = 0;
    for (int i = 0; csvStr[i] != '\n'; i++)
    {
        if (csvStr[i] == ',')
        {
            comma++;
        }
    }
    for (int i = 0; csvStr[i] != '\0'; i++)
    {
        if (csvStr[i] == '\n')
        {
            n++;
        }
    }
    comma++;
    n++;
    StringTable table{n, comma};
    while (table.size_rows() - 1 > row)
    {
        size_t pn = csvStr.find_first_of('\n');
        size_t pcomma = csvStr.find_first_of(',');
        if (col > table.size_columns() - 1)
        {
            cerr << " v wrong number of columns" << endl;
            exit(1);
        }
        if (pcomma < pn)
        {
            string s = csvStr.substr(0, pcomma);
            table.at(row, col) += s;
            col++;
            csvStr = csvStr.substr(pcomma + 1);
        }
        else
        {
            if (col < table.size_columns() - 1)
            {
                cerr << "d wrong number of columns" << endl;
                exit(1);
            }
            string s = csvStr.substr(0, pn);
            table.at(row, col) += s;
            col = 0;
            row++;
            csvStr = csvStr.substr(pn + 1);
        }
    }
    size_t p0 = csvStr.find_first_of('\0');
    while (csvStr.find_first_of(',') < p0)
    {
        size_t pcomma = csvStr.find_first_of(',');
        string s = csvStr.substr(0, pcomma);
        table.at(row, col) += s;
        col++;
        csvStr = csvStr.substr(pcomma + 1);
    }
    string s = csvStr.substr(0, p0);
    table.at(row, col) += s;
    return table;
}

string Csv_toString(StringTable &table)
{
    string str;
    for (size_t i = 0; i < table.size_rows(); i++)
    {
        for (size_t j = 0; j < table.size_columns(); j++)
        {
            str = str + table.at(i, j);
            if (j < table.size_columns() - 1)
            {
                str = str + ',';
            }
        }
        if (table.size_rows() - 1 > i)
        {
            str = str + '\n';
        }
    }
    return str;
}
