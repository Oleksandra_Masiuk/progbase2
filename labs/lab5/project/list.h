#include <cstdlib>
#include <iostream>
#include "../libcsvlab/string_table.h"
#include "dino.h"
#include "era.h"
#include <optional>
#include <vector>
using namespace std;
#pragma once


vector<Dinosaur> createDinoListFromTable(StringTable &csvTable);
StringTable createTableFromList(vector<Dinosaur> v, size_t cols);
vector<Era> createEraListFromTable(StringTable &csvTable);
StringTable createTableFromEraList(vector<Era> v, size_t cols);