#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <ctype.h>
#include <string>
#include <iostream>
using namespace std;
#pragma once
struct Era
{
    string name;
    int from;
    int to;
    int id;
};