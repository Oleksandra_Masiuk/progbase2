#include <string>
#include <vector>
#include <fstream>

#include <optional>
#include "../libcsvlab/string_table.h"
#include "fs.h"
#include "list.h"
#include "dino.h"
#include "era.h"
#pragma once
using namespace std;

class FileStorage
{
   string  dir_name_;

   fstream dinosaurs_file_;
   fstream eras_file_;

   vector<Dinosaur> loadDinosaurs();
   void saveDinosaurs(const vector<Dinosaur> & dinosaurs);
   int getNewDinosaurId();

   vector<Era> loadEras();
   void saveEras(const vector<Era> & eras);
   int getNewEraSId();

 public:
   explicit FileStorage(const string & dir_name = "");
   void setName(const string & dir_name);
   string name() const;

   bool isOpen() const;
   bool open(); 
   void close();

   vector<Dinosaur> getAllDinosaurs(void);
   optional<Dinosaur> getDinosaurById(int dinosaur_id);
   bool updateDinosaur(const Dinosaur &dinosaur);
   bool removeDinosaur(int dinosaur_id);
   int insertDinosaur(const Dinosaur &dinosaur);

   vector<Era> getAllEras(void);
   optional<Era> getEraById(int era_id);
   bool updateEra(const Era &era);
   bool removeEra(int era_id);
   int insertEra(const Era &era);
};
