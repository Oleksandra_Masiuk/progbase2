#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include "fs.h"
#include "dino.h"
#include "storage.h"
#include "cui.h"
#include <optional>
#include <vector>
using namespace std;

int main()
{
    FileStorage storage{"../../data/"};
    bool opt = storage.open();
    Cui cui{&storage};
    cui.show();
    storage.close();
}
