#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dynamic_array.h"
#pragma once
class List
{
    DynamicArray array_; // dynamic array of T elements
    size_t size_;

public:
    List();
    ~List();
    size_t size(); // return number of items in list

    struct Dinosaur get(int index);                // +return self->items[index]
    void set(int index, struct Dinosaur value);    //+ set self->items[index]
    void push_back(struct Dinosaur value); // +insert back
    void remove(double value);    // remove first by value
    void clear();            // make list empty
    void print ();
    void remove_at(int index);
};

