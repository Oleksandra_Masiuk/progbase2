#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dynamic_array.h"
#include "dino.h"
DynamicArray::DynamicArray()
{
    _capacity = 16;
    _items = new struct Dinosaur[_capacity];
}
DynamicArray::~DynamicArray()
{
    delete[] _items;
}
void DynamicArray::resize()
{
    struct Dinosaur *_items2;
    _items2 = new struct Dinosaur[_capacity * 2];
    for (int i = 0; (int)i < _capacity; i++)
    {
        _items2[i].length = _items[i].length;
        _items2[i].weight =_items[i].weight;
        _items2[i].name =_items[i].name;
        _items2[i].order = _items[i].order;
    }
    _capacity = _capacity * 2;
    delete _items;
    _items = _items2;
}
size_t DynamicArray::size()
{
    return _capacity;
}
void DynamicArray::print(int length)
{
    std::cout << "\n" << "dynarray:" <<  std::endl;
    for(int i=0; i<length;i++)
    {
        std::cout << "name: " <<_items[i].name  << " " ;
        std::cout << "length: " <<_items[i].length  << " " ;
        std::cout << "weight: " <<_items[i].weight  << " " ;
        std::cout << "order: " <<_items[i].order  << " " ;
        std::cout << std::endl;
    }    
}
struct Dinosaur DynamicArray::get(int index)
{
    if (index >= _capacity)
    {
        std::cerr << "wrong index" << std::endl;
    }
    else
    {
        return _items[index];
    }
}
void DynamicArray::set(int index, struct Dinosaur value)
{
    if (index < 0)
    {
        std::cout << "wrong index" << std::endl;
    }
    _items[index].length = value.length;
    _items[index].weight = value.weight;
    _items[index].name = value.name;
    _items[index].order = value.order;
}