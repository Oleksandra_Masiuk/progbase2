#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include "fs.h"
#include "string_table.h"
#include "csv.h"
#include "dyn_list.h"
using namespace std;

List createDinoListFromTable(StringTable &csvTable)
{
    List list;
    for (int i = 0; i < csvTable.size_rows(); i++)
    {
        double weight = stod(csvTable.at(i, 1));
        double length = stod(csvTable.at(i, 2));
        Dinosaur d{csvTable.at(i, 0), length, weight, csvTable.at(i, 3)};
        list.push_back(d);
    }
    return list;
}

StringTable createTableFromList( List & list, size_t cols)
{
    StringTable table{list.size(), cols };
    for (int i=0; i<list.size(); i++)
    {
        string name=list.get(i).name;
        string order=list.get(i).order;
        string length=to_string(list.get(i).length);
        string weigth=to_string(list.get(i).weight);
        table.add(0, i, name);
        table.add(1, i,length );
        table.add(2, i, weigth);
        table.add(3, i, order);
    }
    return table;
}

int main()
{
    string name = "data.csv";
    StringTable table = readfile(name);
    table.print();
    size_t cols=table.size_columns();
    List list = createDinoListFromTable(table);
    list.print();
    cout<< endl << "enter N" << endl;
    double N;
    cin >> N;
    list.remove(N);
    list.print();
    StringTable newtable = createTableFromList(list, cols);
    newtable.print();
    string csvtable=Csv_toString(newtable);
    string name2="out.csv";
    writefile(name2, csvtable);
}