#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dino.h"
#pragma once
class DynamicArray
{
  struct Dinosaur * _items;
  size_t _capacity;

public:
  DynamicArray();
  ~DynamicArray();

  size_t size();
  void resize();

struct Dinosaur get(int index);
void set(int index, struct Dinosaur d);
void print(int length);
};

