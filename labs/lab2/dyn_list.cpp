#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dynamic_array.h"
#include <math.h>
#include "dyn_list.h"
#include <string>
using namespace std;
List::List()
{
    DynamicArray array_; // dynamic array of T elements
    size_ = 0;
}
List::~List()
{
    size_ = 0;
}
void List::print()
{
    array_.print(size_);
}
size_t List::size()
{
    return size_;
}
struct Dinosaur List::get(int index)
{
    if (index < size_)
    {
        return array_.get(index);
    }
    else
    {
        std::cerr << "wrong index" << std::endl;
    }
}
void List::set(int index, struct Dinosaur value)
{
    if (index < size_)
    {
        array_.set(index, value);
    }
    else
    {
        std::cerr << "wrong index" << std::endl;
    }
}

void List::remove_at(int index)
{
    if (index < 0)
    {
        std::cerr << "wrong index" << std::endl;
    
    }
    else
    {
        for (int i = index; i < size_ - 1; i++)
        {
            array_.set(i, array_.get(i + 1));
        }
        size_--;
    }
    
}
void List::push_back(struct Dinosaur value)
{
    if (size_ + 1 == array_.size())
    {
        array_.resize();
    }
    array_.set(size_, value);
    size_++;
}
void List::remove(double value)
{
    int i = 0;
    while (i!=size_)
    {
        if (array_.get(i).weight >= value)
        {
            remove_at(i);
        }
        else
        {
            i++;
        }
    }
}
void List::clear()
{
    size_ = 0;
}