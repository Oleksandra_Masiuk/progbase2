#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <ctype.h>
#include <string>
#include <iostream>
using namespace std;
#pragma once
struct Dinosaur 
{
    string name;
    double weight;
    double length;
    string order;
};