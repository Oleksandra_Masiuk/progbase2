#include "edit.h"
#include "ui_edit.h"


Edit::Edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Edit)
{
    ui->setupUi(this);
}

Edit::~Edit()
{
    delete ui;
}
void Edit::setdata(Storage *storage)
{
    storage_=storage;
}
void Edit::setEra()
{
    vector<Era> e=storage_->getAllEras();
    ui->listWidget_2->clear();
    ui->add_era_Button->setEnabled(false);
    ui->remove_era_Button->setEnabled(false);
    for(auto it=e.begin(); it!=e.end(); it++)
    {
        QListWidget * listWidget=ui->listWidget_2;
        QVariant var=QVariant::fromValue((*it));
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(it->name));
        new_item->setData(Qt::UserRole, var);
        listWidget->addItem(new_item);
    }
}
Dinosaur Edit::data()
{
    Dinosaur dino;
    dino.name = ui->lineEdit_name->text().toStdString();
    dino.order = ui->lineEdit_order->text().toStdString();
    dino.weight = ui->double_weight->value();
    dino.length = ui->double_length->value();
    return dino;
}
void Edit::user_value(const Dinosaur &dino)
{
    ui->lineEdit_name->setText(QString::fromStdString(dino.name));
    ui->lineEdit_order->setText(QString::fromStdString(dino.order));
    ui->double_length->setValue(dino.length);
    ui->double_weight->setValue(dino.weight);
    vector <Era> era=storage_->getAllDinosaurEras(dino.id);
    ui->listWidget->clear();
    dino_id=dino.id;
    for(auto it=era.begin(); it!=era.end(); it++)
    {
        QListWidget *listWidget=ui->listWidget;
        QVariant var= QVariant::fromValue((*it));
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(it->name));
        new_item->setData(Qt::UserRole, var);
        listWidget->addItem(new_item);
    }

}

int Edit::getEraid(QString name)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM eras WHERE name = :name");
    query.bindValue(":name", name);
    if(!query.exec())
    {
        qDebug()<<"Get era error"<<query.lastError();
        return 0;
    }
    if (query.next())
    {
        int era_id=query.value("id").toInt();
        return era_id;
    }
    else
    {
        return 0;
    }
}

void Edit::on_add_era_Button_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget_2->selectedItems();
    if(items.count()==1)
    {
        QListWidgetItem * selectedItem = items.at(0);
        QVariant var = selectedItem->data(Qt::UserRole);
        Era tmp = var.value<Era>();
        vector<Era> eras=storage_->getAllDinosaurEras(dino_id);
        int count =0;
        for(auto it=eras.begin(); it!=eras.end(); it++)
        {
            QVariant var2 = QVariant::fromValue((*it));
            Era tmp2=var2.value<Era>();
            if(tmp.id==tmp2.id)
            {
                QMessageBox::information(this, "Error", "This era is already here");
                count++;
                break;
            }
        }
        if(count==0)
        {
            int era_id=tmp.id;
            if(era_id!=0)
            {
                bool inserted=storage_->insertDinosaurEra(dino_id, era_id);
                if(!inserted)
                {
                    QMessageBox::information(this, "Error", "Cannot insert this Era");
                }
                else
                {
                    optional<Era> tmp=storage_->getEraById(era_id);
                    if(tmp)
                    {
                        Era e =tmp.value();
                        QString inputText=QString::fromStdString(e.name);
                        QListWidgetItem * new_item= new QListWidgetItem(inputText);
                        QVariant var=QVariant::fromValue(e);
                        new_item->setData(Qt::UserRole, var);
                        ui->listWidget->addItem(new_item);
                    }
                }
            }
            else
            {
                QMessageBox::information(this, "Error", "Cannot insert this Era");

            }
        }
    }

}


void Edit::on_remove_era_Button_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if(items.count()>0)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
                    this,
                    "On remove",
                    "Are you sure?",
                    QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
        {
            bool removed=true;
            QListWidgetItem * selectedItem = items.at(0);
            QVariant var = selectedItem->data(Qt::UserRole);
            Era tmp = var.value<Era>();
            qDebug()<<dino_id;
            removed=storage_->removeDinosaurEra(dino_id, tmp.id);
            delete ui->listWidget->takeItem(ui->listWidget->row(selectedItem));
            if(removed)
            {
                QMessageBox::information(this, "Removed", "era was removed");
                if(ui->listWidget->selectedItems().count()== 0)
                {
                    ui->remove_era_Button->setEnabled(false);
                }
            }
            else
            {
                QMessageBox::information(this, "error", "era was not removed");
            }
        }
    }
}

void Edit::on_listWidget_itemClicked(QListWidgetItem *item)
{
    ui->remove_era_Button->setEnabled(true);
}

void Edit::on_listWidget_2_itemClicked(QListWidgetItem *item)
{
    ui->add_era_Button->setEnabled(true);
}
