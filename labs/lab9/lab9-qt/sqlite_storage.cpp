#include "sqlite_storage.h"
#include <QSqlQuery>
#include <QtSql>
#include <QDebug>

SqliteStorage::SqliteStorage(const string &dir_name):dir_name_(dir_name)
{
    db_=QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::isOpen() const
{
    return db_.isOpen();
}
bool SqliteStorage::open()
{
    QString path=QString::fromStdString(this->dir_name_)+"/data.sqlite";
    db_.setDatabaseName(path);
    bool connected =db_.open();
    qDebug()<<path;
    qDebug()<<connected;
    if(!connected)
    {
        return false;
    }
    return true;
}
void SqliteStorage::close()
{
    db_.close();
}

Dinosaur getDinosaurFromQuerry(const QSqlQuery & query)

{
    int id=query.value("id").toInt();
    string name=query.value("name").toString().toStdString();
    double length=query.value("length_dino").toDouble();
    double weight=query.value("weight").toDouble();
    string order = query.value("order_dino").toString().toStdString();
    Dinosaur dino;
    dino.id=id;
    dino.name=name;
    dino.length=length;
    dino.weight=weight;
    dino.order=order;
    return dino;
}
vector<Dinosaur> SqliteStorage::getAllDinosaurs(void)
{
    QSqlQuery query("SELECT * FROM dinosaurs");
    vector<Dinosaur> dinosaurs;
    while (query.next())
    {
       Dinosaur dino=getDinosaurFromQuerry(query);
       dinosaurs.push_back(dino);
    }
    return dinosaurs;
}
optional<Dinosaur> SqliteStorage::getDinosaurById(int dinosaur_id)
{
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM dinosaurs WHERE id = :id"))
    {
       qDebug() << "get dinosaur query prepare error:" << query.lastError();
       return nullopt;
    }
    query.bindValue(":id", dinosaur_id);
    if (!query.exec())
    {  // do exec if query is prepared SELECT query
       qDebug() << "get dinosaur query exec error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
       Dinosaur dino=getDinosaurFromQuerry(query);
       return dino;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }
}
bool SqliteStorage::updateDinosaur(const Dinosaur &dinosaur)
{
    QSqlQuery query;
    query.prepare("UPDATE dinosaurs SET name = :name, length_dino = :length_dino, weight = :weight, order_dino = :order_dino WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(dinosaur.name));
    query.bindValue(":length_dino", dinosaur.length);
    query.bindValue(":weight", dinosaur.weight);
    query.bindValue(":order_dino",  QString::fromStdString(dinosaur.order));
    query.bindValue(":id", dinosaur.id);
    if (!query.exec())
    {
        qDebug() << "updateDinosaur query exec error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
bool SqliteStorage::removeDinosaur(int dinosaur_id)
{
    QSqlQuery query;
    if (!query.prepare("DELETE FROM dinosaurs WHERE id = :id"))
    {
        qDebug() << "deleteDino query prepare error:" << query.lastError();
        return false;
    }
    query.bindValue(":id", dinosaur_id);
    if (!query.exec())
    {
        qDebug() << "deleteDino query exec error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertDinosaur(const Dinosaur &dinosaur)
{
    QSqlQuery query;
    query.prepare("INSERT INTO dinosaurs (name, length_dino, weight, order_dino) VALUES (:name, :length_dino, :weight, :order_dino)");
         QSqlError error = query.lastError();
    query.bindValue(":name", QString::fromStdString(dinosaur.name));
    query.bindValue(":length_dino", dinosaur.length);
    query.bindValue(":weight", dinosaur.weight);
    query.bindValue(":order_dino", QString::fromStdString(dinosaur.order));
    if (!query.exec())
    {
       qDebug() << "addDino query exec error:"
                << query.lastError();
       return 0;
    }
    QVariant var= query.lastInsertId();
    return var.toInt();
}
Era getEraFromQuerry(const QSqlQuery & query)
{
    int id=query.value("id").toInt();
    string name=query.value("name").toString().toStdString();
    int from=query.value("from_year").toInt();
    int to=query.value("to_year").toInt();
    Era era;
    era.id=id;
    era.name=name;
    era.from=from;
    era.to=to;
    return era;
}
vector<Era> SqliteStorage::getAllEras(void)
{
    QSqlQuery query("SELECT * FROM eras");
    vector<Era> eras;
    while (query.next())
    {
       Era era=getEraFromQuerry(query);
       eras.push_back(era);
    }
    return eras;
}
optional<Era> SqliteStorage::getEraById(int era_id)
{
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM eras WHERE id = :id"))
    {
       qDebug() << "get era query prepare error:" << query.lastError();
       return nullopt;
    }
    query.bindValue(":id", era_id);
    if (!query.exec())
    {  // do exec if query is prepared SELECT query
       qDebug() << "get era query exec error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
       Era era=getEraFromQuerry(query);
       return era;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }
}
bool SqliteStorage::updateEra(const Era &era)
{
    QSqlQuery query;
    if (!query.prepare("UPDATE eras SET name = :name, from_year = :from_year, to_year = :to_year WHERE id = :id"))
    {
        qDebug() << "updateEra query prepare error:" << query.lastError();
        return false;
    }
    query.bindValue(":name", QString::fromStdString(era.name));
    query.bindValue(":from_year", era.from);
    query.bindValue(":to_year", era.to);
    query.bindValue(":id", era.id);
    if (!query.exec())
    {
        qDebug() << "updateEra query exec error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
bool SqliteStorage::removeEra(int era_id)
{
    QSqlQuery query;
    if (!query.prepare("DELETE FROM eras WHERE id = :id"))
    {
        qDebug() << "deleteEra query prepare error:" << query.lastError();
        return false;
    }
    query.bindValue(":id", era_id);
    if (!query.exec())
    {
        qDebug() << "deleteEra query exec error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertEra(const Era &era)
{
    QSqlQuery query;
    query.prepare("INSERT INTO eras (name, from_year, to_year) VALUES (:name, :from_year, :to_year)");
    query.bindValue(":name", QString::fromStdString(era.name));
    query.bindValue(":from_year", era.from);
    query.bindValue(":to_year", era.to);
    if (!query.exec())
    {
       qDebug() << "addEra query exec error:"
                << query.lastError();
       return 0;
    }
    QVariant var= query.lastInsertId();
    return var.toInt();
}

optional<User> SqliteStorage::getUserAuth( const string & username, const string & password)
{
    qDebug()<<QString::fromStdString(username);
    qDebug()<<QString::fromStdString(password);
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username = :username AND password_hash=:password_hash");
    query.bindValue(":username", QString::fromStdString(username));
    query.bindValue(":password_hash", QString::fromStdString(password));
    if(!query.exec())
    {
        qDebug()<<"Get user error"<<query.lastError();
        return nullopt;
    }
    if(query.next())
    {
        User u;
        u.id=query.value("id").toInt();
        u.username=query.value("username").toString().toStdString();
        u.password_hash= query.value("password_hash").toString().toStdString();
        return u;
    }
    else
    {
        return nullopt;
    }
}
vector<Dinosaur> SqliteStorage::getAllUserDinosaurs(int user_id)
{
    vector <Dinosaur> dinosaurs;
    QSqlQuery query;
    query.prepare("SELECT * FROM dinosaurs WHERE user_id= :user_id");
    query.bindValue(":user_id", user_id);
    if(!query.exec())
    {
        qDebug()<<"Get dino error"<<query.lastError();
    }
    while (query.next())
    {
        Dinosaur d=getDinosaurFromQuerry(query);
        dinosaurs.push_back(d);
    }
    return dinosaurs;
}


vector<Era> SqliteStorage::getAllDinosaurEras(int dinosaur_id)
{
    vector<Era> eras;
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE dino_id= :dino_id");
    query.bindValue(":dino_id", dinosaur_id);
    if(!query.exec())
    {
        qDebug()<<"Get link error"<<query.lastError();
    }
    while (query.next())
    {
        int era_id = query.value("era_id").toInt();
        optional<Era> temp = getEraById(era_id);
        if (temp)
        {

            eras.push_back(temp.value());

        }
    }
    return eras;
}
bool SqliteStorage::insertDinosaurEra(int dinosaur_id, int era_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO links (dino_id, era_id) VALUES ( :dino_id, :era_id)");
    query.bindValue(":dino_id", dinosaur_id);
    query.bindValue(":era_id", era_id);
    if (!query.exec())
    {
        qDebug() << "insert links query error:" << query.lastError();
        return false;
    }
    return true;
}
bool SqliteStorage::removeDinosaurEra(int dinosaur_id, int era_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE dino_id=:dino_id AND era_id=:era_id");
    query.bindValue(":dino_id", dinosaur_id);
    query.bindValue(":era_id", era_id);
    if (!query.exec()){
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
bool SqliteStorage::addUserid(int user_id, int id)
 {

     QSqlQuery query;
     query.prepare("UPDATE dinosaurs SET user_id=:user_id WHERE id = :id");
     query.bindValue(":user_id", user_id);
     query.bindValue(":id", id);

     if (!query.exec()){
         qDebug() << "Update users query error:" << query.lastError();
         return false;
     }
     else if(query.numRowsAffected()==0)
     {
         return false;
     }
     return true;
 }
