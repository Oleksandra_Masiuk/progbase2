#ifndef EDIT_H
#define EDIT_H

#include <QDialog>
#include <QAction>
#include <QMessageBox>
#include <QVariant>
#include <QVector>
#include <QString>
#include <QListWidgetItem>
#include <QFileDialog>
#include "sqlite_storage.h"

namespace Ui {
class Edit;
}

class Edit : public QDialog
{
    Q_OBJECT

public:
    explicit Edit(QWidget *parent = 0);
    void setdata(Storage *storage);
    void user_value(const Dinosaur &dino);
    Dinosaur data();
    void setEra();
    int dino_id;
    ~Edit();

private slots:
    void on_add_era_Button_clicked();

    void on_remove_era_Button_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_listWidget_2_itemClicked(QListWidgetItem *item);

private:
    Ui::Edit *ui;
    Storage *storage_;
    int getEraid(QString name);

};

#endif // EDIT_H
