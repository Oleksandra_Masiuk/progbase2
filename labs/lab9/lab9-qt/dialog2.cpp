#include "dialog2.h"
#include "ui_dialog2.h"
#include <QDebug>

Dialog2::Dialog2(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog2)
{
    ui->setupUi(this);
}
void Dialog2::setdata(Storage *storage)
{
    storage_=storage;
}
Dialog2::~Dialog2()
{
    delete ui;
}
QString Dialog2::hashPassword (QString const & pass)
{
    QByteArray pass_ba =pass.toUtf8();
    QByteArray hash_ba =QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
    QString pass_hash=QString(hash_ba.toHex());
    return pass_hash;
}


void Dialog2::on_ok_b_clicked()
{
    user.username=ui->login->text().toStdString();
    user.password_hash=this->hashPassword((ui->password->text())).toStdString();
    optional u_opt=storage_->getUserAuth(user.username, user.password_hash);
    if(u_opt)
    {
        this->user_id=u_opt.value().id;
        QMessageBox::information(this, "Success", "User confirmed");
        this->close();
        click=true;
    }
    else
    {
        QMessageBox::information(this,"Error", "Wrong login or password");
        click=false;
    }
}


void Dialog2::on_cancel_b_clicked()
{
    QMessageBox::information(this,"User unconfirmed", "Canceled authentication");
    this->close();
    click=false;
}
