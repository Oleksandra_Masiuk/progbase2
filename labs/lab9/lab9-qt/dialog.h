#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "dino.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    Dinosaur data();
    void user_value(const Dinosaur & dino);


private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
