#ifndef DIALOG2_H
#define DIALOG2_H

#include <QDialog>
#include <QString>
#include "user.h"
#include <QCryptographicHash>
#include <QMessageBox>
#include <QAction>
#include "storage.h"
#include "sqlite_storage.h"
using namespace std;

namespace Ui {
class Dialog2;
}

class Dialog2 : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog2(QWidget *parent = 0);//, Storage *storage=nullptr);
    ~Dialog2();
    void setdata(Storage *storage);
     QString hashPassword (QString const & pass);
     bool click;
     int user_id;

private slots:
     void on_ok_b_clicked();

     void on_cancel_b_clicked();

private:
    Ui::Dialog2 *ui;
    Storage *storage_;
    User user;
};

#endif // DIALOG2_H
