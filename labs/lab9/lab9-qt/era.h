#ifndef ERA_H
#define ERA_H

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <ctype.h>
#include <string>
#include <iostream>
#include <QVariant>
using namespace std;
#pragma once

struct Era
{
    string name;
    int from;
    int to;
    int id;
};
Q_DECLARE_METATYPE(Era)
#endif // ERA_H
