#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "storage.h"
#include <QDebug>
#include <QFileDialog>
#include <QListWidgetItem>
#include "dialog.h"
#include "sqlite_storage.h"
#include <QMessageBox>
#include <QDialog>
#include "dialog2.h"
#include "edit.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int user_id;

private slots:
    void onOpenStorage();

    void Logout();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void visibility();

    void on_Add_clicked();

    void on_Remove_clicked();

    void on_Edit_clicked();

private:

    Ui::MainWindow *ui;
    Storage * storage_;
    void dino_details(const Dinosaur & dino);

};

#endif // MAINWINDOW_H
