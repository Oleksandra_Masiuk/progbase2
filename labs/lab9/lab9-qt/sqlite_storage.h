#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H
#pragma once
using namespace std;
#include <string>
#include <vector>
#include <QtSql>
#include <QSqlDatabase>
#include <optional>

#include "storage.h"

class SqliteStorage:public Storage
{
protected:
   const string dir_name_;
   QSqlDatabase db_;
public:
   SqliteStorage(const string & dir_name) ;//: Storage(dir_name) {}

   bool isOpen() const;
   bool open();
   void close();

   vector<Dinosaur> getAllDinosaurs(void);
   optional<Dinosaur> getDinosaurById(int dinosaur_id);
   bool updateDinosaur(const Dinosaur &dinosaur);
   bool removeDinosaur(int dinosaur_id);
   int insertDinosaur(const Dinosaur &dinosaur);

   vector<Era> getAllEras(void);
   optional<Era> getEraById(int era_id);
   bool updateEra(const Era &era);
   bool removeEra(int era_id);
   int insertEra(const Era &era);

   optional<User> getUserAuth( const string & username, const string & password);
   vector<Dinosaur> getAllUserDinosaurs(int user_id);


   vector<Era> getAllDinosaurEras(int dinosaur_id);
   bool insertDinosaurEra(int dinosaur_id, int era_id);
   bool removeDinosaurEra(int dinosaur_id, int era_id);
   bool addUserid(int user_id, int id);
};

#endif // SQLITE_STORAGE_H
