/********************************************************************************
** Form generated from reading UI file 'edit.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_H
#define UI_EDIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Edit
{
public:
    QDialogButtonBox *buttonBox;
    QLabel *label;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QVBoxLayout *verticalLayout_5;
    QLineEdit *lineEdit_name;
    QDoubleSpinBox *double_weight;
    QDoubleSpinBox *double_length;
    QLineEdit *lineEdit_order;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QListWidget *listWidget;
    QListWidget *listWidget_2;
    QPushButton *add_era_Button;
    QPushButton *remove_era_Button;

    void setupUi(QDialog *Edit)
    {
        if (Edit->objectName().isEmpty())
            Edit->setObjectName(QStringLiteral("Edit"));
        Edit->resize(906, 309);
        buttonBox = new QDialogButtonBox(Edit);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(500, 260, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        label = new QLabel(Edit);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(30, -30, 121, 16));
        horizontalLayoutWidget = new QWidget(Edit);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(20, 60, 241, 151));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        label_2 = new QLabel(horizontalLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_6->addWidget(label_2);

        label_3 = new QLabel(horizontalLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_6->addWidget(label_3);

        label_4 = new QLabel(horizontalLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout_6->addWidget(label_4);

        label_5 = new QLabel(horizontalLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout_6->addWidget(label_5);


        horizontalLayout->addLayout(verticalLayout_6);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        lineEdit_name = new QLineEdit(horizontalLayoutWidget);
        lineEdit_name->setObjectName(QStringLiteral("lineEdit_name"));

        verticalLayout_5->addWidget(lineEdit_name);

        double_weight = new QDoubleSpinBox(horizontalLayoutWidget);
        double_weight->setObjectName(QStringLiteral("double_weight"));

        verticalLayout_5->addWidget(double_weight);

        double_length = new QDoubleSpinBox(horizontalLayoutWidget);
        double_length->setObjectName(QStringLiteral("double_length"));

        verticalLayout_5->addWidget(double_length);

        lineEdit_order = new QLineEdit(horizontalLayoutWidget);
        lineEdit_order->setObjectName(QStringLiteral("lineEdit_order"));

        verticalLayout_5->addWidget(lineEdit_order);


        horizontalLayout->addLayout(verticalLayout_5);

        label_6 = new QLabel(Edit);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(40, 30, 121, 16));
        label_7 = new QLabel(Edit);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(340, 30, 121, 16));
        label_8 = new QLabel(Edit);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(650, 30, 121, 16));
        listWidget = new QListWidget(Edit);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(310, 60, 231, 151));
        listWidget_2 = new QListWidget(Edit);
        listWidget_2->setObjectName(QStringLiteral("listWidget_2"));
        listWidget_2->setGeometry(QRect(620, 70, 231, 151));
        add_era_Button = new QPushButton(Edit);
        add_era_Button->setObjectName(QStringLiteral("add_era_Button"));
        add_era_Button->setGeometry(QRect(310, 230, 89, 25));
        remove_era_Button = new QPushButton(Edit);
        remove_era_Button->setObjectName(QStringLiteral("remove_era_Button"));
        remove_era_Button->setGeometry(QRect(440, 230, 89, 25));

        retranslateUi(Edit);
        QObject::connect(buttonBox, SIGNAL(accepted()), Edit, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Edit, SLOT(reject()));

        QMetaObject::connectSlotsByName(Edit);
    } // setupUi

    void retranslateUi(QDialog *Edit)
    {
        Edit->setWindowTitle(QApplication::translate("Edit", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("Edit", "Dino data:", Q_NULLPTR));
        label_2->setText(QApplication::translate("Edit", "Name", Q_NULLPTR));
        label_3->setText(QApplication::translate("Edit", "Weight", Q_NULLPTR));
        label_4->setText(QApplication::translate("Edit", "Length", Q_NULLPTR));
        label_5->setText(QApplication::translate("Edit", "Order", Q_NULLPTR));
        lineEdit_name->setPlaceholderText(QApplication::translate("Edit", "dino", Q_NULLPTR));
        lineEdit_order->setPlaceholderText(QApplication::translate("Edit", "order", Q_NULLPTR));
        label_6->setText(QApplication::translate("Edit", "Dino data:", Q_NULLPTR));
        label_7->setText(QApplication::translate("Edit", "Dino's era(s):", Q_NULLPTR));
        label_8->setText(QApplication::translate("Edit", "All era(s):", Q_NULLPTR));
        add_era_Button->setText(QApplication::translate("Edit", "add", Q_NULLPTR));
        remove_era_Button->setText(QApplication::translate("Edit", "remove", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Edit: public Ui_Edit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_H
