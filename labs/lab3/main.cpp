#include "options.h"
#include "dino.h"
#include "bstree.h"
#include "dyn_list.h"
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include "fs.h"
#include "string_table.h"
#include "csv.h"
using namespace std;

List<Dinosaur> createDinoListFromTable(StringTable &csvTable);
void printDinoList(List<Dinosaur> &list);
StringTable createTableFromList(List<Dinosaur> &list, size_t cols);

int main(int argc, char *argv[])
{
    Options opt = getProgramOptions(argc, argv);
    StringTable table = readfile(opt.input_file_name);
    table.print();
    List<Dinosaur> list = createDinoListFromTable(table);
    printDinoList(list);
    BSTree tree;
    if (opt.build_bstree == true)
    {
        for (int i = 0; i < list.size(); i++)
        {
            tree.insert(list.at(i));
        }
        cout<<endl;
        tree.print();
    }
    int len=0;
    int id[list.size()];
    if (!isnan(opt.n_process))
    {
        double N = opt.n_process;
        int i = 0;
        while (i != list.size())
        {
            if (list.at(i).weight >= N)
            {
                id[len]=list.at(i).id;
                len++;
                list.remove_at(i);
            }
            else
            {
                i++;
            }
        }
        printDinoList(list);
        if (opt.build_bstree == true)
        {
            for (int i = 0; i < len; i++)
            {
                tree.remove(id[i]);
            }
            tree.print();
            tree.clear();
        }
    }
    StringTable newtable = createTableFromList(list, table.size_columns());
    newtable.print();
    if (opt.output_file_name != "")
    {
        string csvtable = Csv_toString(newtable);
        string name2 = opt.output_file_name;
        writefile(name2, csvtable);
    }
}
void printDinoList(List<Dinosaur> &list)
{
    cout << endl
         << "dynarray:" << endl;
    for (int i = 0; i < list.size(); i++)
    {
        cout << "id: " << list.at(i).id << " ";
        cout << "name: " << list.at(i).name << " ";
        cout << "length: " << list.at(i).length << " ";
        cout << "weight: " << list.at(i).weight << " ";
        cout << "order: " << list.at(i).order << " ";
        cout << endl;
    }
}
List<Dinosaur> createDinoListFromTable(StringTable &csvTable)
{
    List<Dinosaur> list;
    for (int i = 0; i < csvTable.size_rows(); i++)
    {
        double weight = stod(csvTable.at(i, 2));
        double length = stod(csvTable.at(i, 3));
        int id = stoi(csvTable.at(i, 0));
        Dinosaur d{csvTable.at(i, 1), length, weight, csvTable.at(i, 4), id};
        list.push_back(d);
    }
    return list;
}
StringTable createTableFromList(List<Dinosaur> &list, size_t cols)
{
    StringTable table{list.size(), cols};
    for (int i = 0; i < list.size(); i++)
    {
        string name = list.at(i).name;
        string order = list.at(i).order;
        string length = to_string(list.at(i).length);
        string weigth = to_string(list.at(i).weight);
        table.add(0, i, name);
        table.add(1, i, length);
        table.add(2, i, weigth);
        table.add(3, i, order);
    }
    return table;
}