#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "fs.h"
using namespace std;

StringTable readfile(string &name)
{
    ifstream fin;
    fin.open(name);
    if (!fin.is_open())
    {
        cerr << "file cannot be opened" << endl;
        exit(1);
    }
    string str;
    string s;
    while (!fin.eof())
    {
        getline(fin, s);
        str += s;
        str += '\n';
    }
    size_t len = str.length() - 1;
    str[len] = '\0';
    StringTable t = Csv_parse(str);
    fin.close();
    return t;
}
void writefile(string &name, string &csvtable)
{
    ofstream fout;
    fout.open(name);
    if (!fout.is_open())
    {
        cerr << "file cannot be opened" << endl;
        exit(1);
    }
    fout<<csvtable;
    fout.close();
}
