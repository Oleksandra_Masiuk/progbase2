#include "bintree.h"
#include <cassert>
class BSTree {
    BinTree * root_ = nullptr;
    size_t size_ = 0;

public:
    size_t size     ();  // number of stored values    
    void   insert   ( Dinosaur & value);  // add unique 
    bool   lookup   (int key);  // check for value with a key 
    Dinosaur search   (int key);  // get the value for a key 
    void remove   (int key);  // remove the value for a key
    void   clear    ();           // remove all values
    void print();
    BinTree* root();
};

int getKey(Dinosaur value);  // a function to get key from value 
