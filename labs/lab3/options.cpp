#include "options.h"
using namespace std;

Options getProgramOptions(int argc, char *argv[])
{
    if (argc < 2 || argc > 7)
    {
        cerr << "wrong number of options";
        exit(1);
    }
    Options opt;
    double N;
    string out;
    bool in = false;
    bool n = false;
    bool o = false;
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-n") == 0)
        {
            if (n == true || (atof(argv[i + 1]) == 0 && (strcmp(argv[i + 1], "0") != 0)))
            {
                cerr << "wrong N";
                exit(1);
            }
            else
            {
                opt.n_process = atof(argv[i + 1]);
                i++;
                n = true;
            }
        }
        else if (strcmp(argv[i], "-o") == 0)
        {
            if (o == false)
            {
                if (strcmp(argv[i + 1]+3, ".csv") != 0)
                {
                    opt.output_file_name = "out.csv";
                }
                else
                {
                    opt.output_file_name = argv[i + 1];
                    i++;
                }
            }
            else
            {
                cerr << " there cannot be two output files ";
                exit(1);
            }
        }
        else if (strcmp(argv[i], "-b") == 0)
        {
            opt.build_bstree = true;
        }
        else if ((strchr(argv[i], '-') == 0) && in == false)
        {
            opt.input_file_name = argv[i];
            if ((opt.input_file_name.length() < 5) || opt.input_file_name.substr(opt.input_file_name.length() - 4, opt.input_file_name.length() - 1) != ".csv")
            {
                cerr << " wrong input file name";
                exit(1);
            }
            in = true;
        }
    }
    if (opt.input_file_name.empty())
    {
        cerr << "there is no input file" << endl;
        exit(1);
    }
    return opt;
}