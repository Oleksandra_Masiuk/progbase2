#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include <cstring>
#include <cctype>
using namespace std;
#pragma once
struct Options
{
    string input_file_name="";
    double n_process=NAN;  
    string output_file_name="";
    bool build_bstree=0;
};

Options getProgramOptions(int argc, char * argv[]);