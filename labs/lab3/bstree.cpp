#include "bstree.h"
size_t BSTree::size()
{
    return size_;
}

static void insertNode(BinTree *node, BinTree *newNode)
{
    assert(node != nullptr);
    if (newNode->id == node->id)
    {
        fprintf(stderr, "Key %i already exists in BST\n", newNode->id);
        abort();
    }
    else if (newNode->id < node->id)
    {
        if (node->left == nullptr)
        {
            node->left = newNode;
        }
        else
        {
            insertNode(node->left, newNode);
        }
    }
    else
    {
        if (node->right == nullptr)
        {
            node->right = newNode;
        }
        else
        {
            insertNode(node->right, newNode);
        }
    }
}

void BSTree::insert(Dinosaur &value)
{
    int key = getKey(value);
    BinTree *newNode = new BinTree{key, value};
    this->size_ += 1;
    if (this->root_ == nullptr)
    {
        this->root_ = newNode;
    }
    else
    {
        insertNode(this->root_, newNode);
    }
}

static bool lookupKey(BinTree *node, int key)
{
    if (node == nullptr)
    {
        return false;
    }
    if (key < node->id)
    {
        return lookupKey(node->left, key);
    }
    else if (key > node->id)
    {
        return lookupKey(node->right, key);
    }
    return true;
}

bool BSTree::lookup(int key)
{
    return lookupKey(this->root_, key);
}

int getKey(Dinosaur value)
{
    int key = value.id;
    return key;
}

static Dinosaur searchKey(BinTree *node, int key)
{
    if (node == nullptr)
    {
        cerr << "tree is empty";
        exit(1);
    }
    if (key < node->id)
    {
        return searchKey(node->left, key);
    }
    else if (key > node->id)
    {
        return searchKey(node->right, key);
    }
    return node->value;
}
Dinosaur BSTree::search(int key)
{
    return searchKey(this->root_, key);
}
static void clear_tree(BinTree *node)
{
    if (node == nullptr)
    {
        return;
    }
    clear_tree(node->left);
    clear_tree(node->right);
    delete node;
}
void BSTree::clear()
{
    clear_tree(this->root_);
    this->root_ = nullptr;
}
static void printValueOnLevel(BinTree *node, char pos, int depth)
{

    for (int i = 0; i < depth; i++)
    {
        printf("....");
    }
    printf("%c: ", pos);

    if (node == nullptr)
    {
        printf("(null)\n");
    }
    else
    {
        cout << "id=" << node->value.id << " " << node->value.name << " w=" << node->value.weight << " l=" << node->value.length << endl;
    }
}

static void printNode(BinTree *node, char pos, int depth)
{
    bool hasChild = node != nullptr && (node->left != nullptr || node->right != nullptr);
    if (hasChild)
    {
        printNode(node->right, 'R', depth + 1);
    }
    printValueOnLevel(node, pos, depth);
    if (hasChild)
    {
        printNode(node->left, 'L', depth + 1);
    }
}
static void printBinTree(BinTree *root)
{
    printNode(root, '+', 0);
}
void BSTree::print()
{
    printBinTree(this->root_);
}














static void modifyTreeOnRemove(BinTree *node, BinTree *parent);

static Dinosaur removeNode(BinTree *node, int key, BinTree *parent)
{
    if (node == nullptr)
    {
        fprintf(stderr, "Key `%i` not found on removal\n", key);
        abort();
    }

    if (key < node->id)
    {
        return removeNode(node->left, key, node);
    }
    else if (key > node->id)
    {
        return removeNode(node->right, key, node);
    }
    else
    {
        modifyTreeOnRemove(node, parent);
        Dinosaur removedValue = node->value;
        delete node;
        return removedValue;
    }
}

static BinTree *getRemoveReplacementNode(BinTree *node);

static void modifyTreeOnRemove(BinTree *node, BinTree *parent)
{
    BinTree *replacementNode = getRemoveReplacementNode(node);
    if (parent->left == node)
    {
        parent->left = replacementNode;
    }
    else
    {
        parent->right = replacementNode;
    }
}

static BinTree *modifyTreeOnCaseC(BinTree *node);

static BinTree *getRemoveReplacementNode(BinTree *node)
{
    if (node->left == nullptr && node->right == nullptr)
    {
        return nullptr;
    }
    else if (node->left == nullptr || node->right == nullptr)
    {
        BinTree *child = (node->left != nullptr) ? node->left : node->right;
        return child;
    }
    else
    {
        return modifyTreeOnCaseC(node);
    }
}

static BinTree *searchMin(BinTree *node);

static BinTree *modifyTreeOnCaseC(BinTree *node)
{

    BinTree *minNode = searchMin(node->right);
    int minKey = minNode->id;
    Dinosaur removedValue = removeNode(node->right, minKey, node);
    BinTree *newMin = new BinTree{minKey, removedValue};
    newMin->left = node->left;
    newMin->right = node->right;
    return newMin;
}

static BinTree *searchMin(BinTree *node)
{
    if (node == nullptr)
    {
        return nullptr;
    }
    if (node->left == nullptr)
    {
        return node;
    }
    return searchMin(node->left);
}
void BSTree::remove(int key) 
{
    Dinosaur d;
    BinTree fakeParent{-1,  d};
    fakeParent.left = this->root_;
    Dinosaur old = removeNode(this->root_, key, &fakeParent);
    this->root_ = fakeParent.left;
    this->size_--;

}
BinTree* BSTree::root()
{
    return this->root_;
}