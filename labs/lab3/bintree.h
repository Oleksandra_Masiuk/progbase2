#include "dino.h"
struct BinTree 
{
   Dinosaur value;      
   BinTree * left = nullptr;  
   BinTree * right = nullptr; 
   int id=0;
  
   BinTree(int i, Dinosaur & val):id{i}, value{val}{}
};