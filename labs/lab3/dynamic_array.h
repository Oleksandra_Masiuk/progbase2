#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cmath>
#pragma once
using namespace std;

template <typename T>

class DynamicArray
{
  T *_items;
  size_t _capacity;

public:
  DynamicArray()
  {
    _capacity = 16;
    _items = new T[_capacity];
  }
  ~DynamicArray()
  {
    delete[] _items;
    _capacity = 0;
  }
  size_t size()
  {
    return _capacity;
  }

  T & at(int index)
  {
    if (index >= _capacity)
    {
      std::cerr << "wrong index" << std::endl;
    }
    else
    {
      return _items[index];
    }
  }
  void resize(size_t cap)
  {
    T *_items2 = new T[cap];
    for (int i = 0; i < fmin(_capacity, cap); i++)
    {
      _items2[i] = _items[i];
    }
    delete[] _items;
    _items = _items2;
    _capacity = cap;
  }
  //void print(int length);
};
