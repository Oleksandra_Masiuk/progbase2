using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dynamic_array.h"
#pragma once
template <typename T>

class List
{
    DynamicArray<T> array_;
    size_t size_;

public:
    List() 
    {
        size_=0;
    }
    ~List() {}
    size_t size()
    {
        return size_;
    }
    T & at(int index)
    {
        if (index < size_)
        {
            return array_.at(index);
        }
        else
        {
            cerr << "wrong index" << endl;
        }
    }
    void push_back(T & value)
    {
        if (size_ + 1 == array_.size())
        {
            array_.resize(size_*2);
        }
        array_.at(size_) = value;
        size_++;
    }
    void remove(T & value)
    {
        int index = index_of(value);
        if (index = -1)
        {
            cerr << "error" << endl;
        }
        remove_at(index);
    }
    void remove_at(int index)
    {
        if (index < 0)
        {
            cerr << "wrong index" << endl;
        }
        else
        {
            for (int i = index; i < size_ - 1; i++)
            {
                array_.at(i)= array_.at(i + 1);
            }
            size_--;
        }
    }
    void insert(int index, T & value)
    {
        size_t cap = array_.size();
        if (size_ == cap)
        {
            array_.resize(size_*2);
        }
        if (index < size_ && index > 0)
        {
            for (int i = size_; i > index; i--)
            {
                array_.at(i) = array_.at(i - 1);
            }
            array_.at(index) = value;
            size_ = size_ + 1;
        }
        else
        {
            cerr << "wrong index" << endl;
        }
    }
    void clear()
    {
        return size_ = 0;
    }
    bool contains(T & value)
    {
        bool contain = false;
        for (size_t i = 0; i < size_; i++)
        {
            if (array_.at(i) == value)
            {
                contain = true;
            }
        }
        return contain;
    }
    bool empty()
    {
        return size_ = 0;
    }
    int index_of(T & value)
    {
        int i = -1;
        for (i = 0; i < size_; i++)
        {
            if (array_.at(i) == value)
            {
                break;
            }
        }
        return i;
    }
};
