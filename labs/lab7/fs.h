#ifndef FS_H
#define FS_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;
#pragma once

void writefile(fstream & fout, string & str);
string readxmlfile (fstream & fin);
#endif // FS_H
