#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    visibility();
    connect(ui->actionNew_storage, &QAction::triggered, this, &MainWindow::onNewStorage);
    connect(ui->actionOpen_storage, &QAction::triggered, this, &MainWindow::onOpenStorage);

}

MainWindow::~MainWindow()
{
    delete storage_;
    delete ui;
}
void MainWindow::onNewStorage()
{
    if(storage_!=nullptr)
    {
        delete storage_;
    }
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "new_storage";
    QString folder_path = dialog.getSaveFileName(this, "Select New Storage Folder", current_dir + "/" + default_name, "Folders");
    if(folder_path.isEmpty())
    {
        return;
    }
    else
    {
        storage_=new XmlStorage{folder_path.toStdString()};
        vector<Dinosaur> dino=storage_->getAllDinosaurs();
        ui->listWidget->clear();
        visibility();
        for(auto item=dino.begin(); item!=dino.end(); item++)
        {
            QListWidget * listWidget = ui->listWidget;
            QString inputText=QString::fromStdString(item->name);
            QListWidgetItem * new_item = new QListWidgetItem(inputText);
            QVariant var=QVariant::fromValue((*item));
            new_item->setData(Qt::UserRole, var);
            listWidget->addItem(new_item);
        }
        ui->Add->setEnabled(true);
    }
}
void MainWindow::onOpenStorage()
{
    if(storage_!=nullptr)
    {
        delete storage_;
    }
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString folder_path=dialog.getExistingDirectory(this, "Select Folder");
    if(folder_path.isEmpty())
    {
        return;
    }
    else
    {
        storage_=new XmlStorage{folder_path.toStdString()};
        vector<Dinosaur> dino=storage_->getAllDinosaurs();
         ui->listWidget->clear();
         visibility();
         for(auto item=dino.begin(); item!=dino.end(); item++)
         {
             QListWidget * listWidget = ui->listWidget;
             QString inputText=QString::fromStdString(item->name);
             QListWidgetItem * new_item = new QListWidgetItem(inputText);
             QVariant var=QVariant::fromValue((*item));
             new_item->setData(Qt::UserRole, var);
             listWidget->addItem(new_item);
         }
         ui->Add->setEnabled(true);
    }
}
void MainWindow::visibility()
{
    bool v;
    int count = ui->listWidget->selectedItems().count();
    if(count != 0)
    {
        v = true;
    }
    else
    {
         v = false;
    }
    ui->widget->setVisible(v);
    ui->label_6->setVisible(v);
}
void MainWindow::dino_details(const Dinosaur & dino) {

    ui->label_id->setText("<h4><i><b>" + QString::number(dino.id) + "</b</i></h4>");
    ui->label_name->setText("<h4><i><b>" + QString::fromStdString(dino.name) + "</b</i></h4>");
    ui->label_order->setText("<h4><i><b>" + QString::fromStdString(dino.order) + "</b</i></h4>");
    ui->label_weight->setText("<h4><i><b>" + QString::number(dino.weight) + "</b</i></h4>");
    ui->label_length->setText("<h4><i><b>" + QString::number(dino.length) + "</b</i></h4>");

}
void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    visibility();
     QVariant var = item->data(Qt::UserRole);
     Dinosaur temp = var.value<Dinosaur>();
     dino_details(temp);
     ui->Edit->setEnabled(true);
     ui->Remove->setEnabled(true);

}

void MainWindow::on_Add_clicked()
{
    QDialog(this);
    if (storage_ != nullptr)
    {
            Dialog addDialog(this);
            addDialog.setWindowTitle("Adding");
            int status = addDialog.exec();
            if (status == 1)
            {

                Dinosaur dino = addDialog.data();
                int dino_id = storage_->insertDinosaur(dino);
                dino.id = dino_id;
                QString inputText = QString::fromStdString(dino.name);
                QListWidgetItem * new_item = new QListWidgetItem(inputText);
                QVariant var = QVariant::fromValue(dino);
                new_item->setData(Qt::UserRole, var);
                ui->listWidget->addItem(new_item);
                QString id_str = "New dino's id: " + QString::number(dino_id);
                QMessageBox::information(this, "Inserted", id_str);

            }

        }
}

void MainWindow::on_Remove_clicked()
{
    QList<QListWidgetItem *> list = ui->listWidget->selectedItems();
    if (list.count()>0)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
            this,
            "On remove",
            "Are you sure?",
            QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
        {
            QListWidgetItem * selectedItem = list.at(0);
            QVariant var = selectedItem->data(Qt::UserRole);
            Dinosaur tmp = var.value<Dinosaur>();
            int t=tmp.id;
            bool removed = storage_->removeDinosaur(t);

            if(removed)
            {
                int row_index = ui->listWidget->row(selectedItem);
                ui->listWidget->takeItem(row_index);
                delete selectedItem;
                QList<QListWidgetItem *> items_1 = ui->listWidget->selectedItems();

                if (items_1.size() == 1)
                {
                    selectedItem = items_1.at(0);
                    QVariant var_1 = selectedItem->data(Qt::UserRole);
                    Dinosaur dino = var_1.value<Dinosaur>();
                    dino_details(dino);
                }
                visibility();
            }
         }
    }
    else if(list.count() == 0)
    {
        ui->Edit->setEnabled(false);
        ui->Remove->setEnabled(false);
    }
}

void MainWindow::on_Edit_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if(items.count() == 1)
    {
        QListWidgetItem * selectedItem = items.at(0);
        QVariant var = selectedItem->data(Qt::UserRole);
        Dinosaur tmp = var.value<Dinosaur>();
        Dialog editDialog(this);
        editDialog.setWindowTitle("Editing");
        editDialog.user_value(tmp);
        int status = editDialog.exec();
        if (status == 1)
        {
            Dinosaur dino = editDialog.data();
            dino.id = tmp.id;
            bool updated = storage_->updateDinosaur(dino);
            if(updated)
            {
                selectedItem->setText(QString::fromStdString(dino.name));
                QVariant tmpvar = QVariant::fromValue(dino);
                selectedItem->setData(Qt::UserRole, tmpvar);
                dino_details(dino);
                QMessageBox::information(this, "Success", "Dino has been updated.");
            }
            else
            {
                QMessageBox::warning(this, "Error", "Can't update this dino.");
            }
       }
    }
    else if(items.count() == 0)
    {
        ui->Edit->setEnabled(false);
        ui->Remove->setEnabled(false);
    }
}
