#include "storage.h"
#include <QString>
#include <QDir>
using namespace std;

FileStorage::FileStorage(const string &dir_name)
{
    dir_name_ = dir_name;
}
void FileStorage::setName(const string &dir_name)
{
    dir_name_ = dir_name;
}
string FileStorage::name() const
{
    return dir_name_;
}

bool FileStorage::isOpen() const
{
    return dinosaurs_file_.is_open();
}
bool FileStorage::open()
{
    QString dirname=QString::fromStdString(name());
    QDir dir(dirname);
    if(!dir.exists())
    {
        dir.mkpath(".");
        vector<Dinosaur> empty;
        saveDinosaurs(empty);
    }
    dinosaurs_file_.open(name() + "/dinosaur.xml");
    return isOpen();
}
void FileStorage::close()
{
    dinosaurs_file_.close();
}

vector<Dinosaur> FileStorage::getAllDinosaurs(void)
{
    return this->loadDinosaurs();
}

optional<Dinosaur> FileStorage::getDinosaurById(int dinosaur_id)
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id == dinosaur_id)
        {
            return dino;
        }
    }
    return nullopt;
}

bool FileStorage::updateDinosaur(const Dinosaur &dinosaur)
{
    int dinosaur_id = dinosaur.id;
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id == dinosaur_id)
        {
            dino = dinosaur;
            this->saveDinosaurs(dinosaurs);
            return true;
        }
    }
    return false;
}

bool FileStorage::removeDinosaur(int dinosaur_id)
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    auto it = dinosaurs.begin();
    auto it_end = dinosaurs.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == dinosaur_id)
        {
            dinosaurs.erase(it);
            this->saveDinosaurs(dinosaurs);
            return true;
        }
    }
    return false;
}

int FileStorage::insertDinosaur(const Dinosaur &dinosaur)
{
    int newId = this->getNewDinosaurId();
    Dinosaur copy = dinosaur;
    copy.id = newId;
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    dinosaurs.push_back(copy);
    this->saveDinosaurs(dinosaurs);
    return newId;
}






