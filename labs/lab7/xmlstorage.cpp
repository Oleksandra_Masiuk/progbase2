#include "xmlstorage.h"
#include "fs.h"

using namespace std;
Dinosaur xmlElementToDino(QDomElement & dinosaur);
QDomElement DinoToXmlElement(QDomDocument & doc, const Dinosaur &dinosaur);

vector<Dinosaur> XmlStorage:: loadDinosaurs()
{

    open();
    string str=readxmlfile(dinosaurs_file_);
    QString xmlText=QString::fromStdString(str);
    vector<Dinosaur> dino;
    QDomDocument doc;
    QString errorMessage;
    if (!doc.setContent(xmlText, &errorMessage))
    {
        qDebug() << "error parsing xml: " << errorMessage;
        exit(1);
    }
    QDomElement rootEl = doc.documentElement();
    QDomNodeList rootElChildren = rootEl.childNodes();
    for(int i=0; i<rootElChildren.length(); i++)
    {
        QDomNode dinoNode=rootElChildren.at(i);
        QDomElement dinoEl =dinoNode.toElement();
        Dinosaur d=xmlElementToDino(dinoEl);
        dino.push_back(d);
    }
    close();
    return dino;
}
void XmlStorage:: saveDinosaurs(const vector<Dinosaur> & dinosaurs)
{
    dinosaurs_file_.open(name()+"/dinosaur.xml", ios::out);
    vector<Dinosaur> dinos=dinosaurs;
    QDomDocument doc;
    QDomElement rootEl = doc.createElement("dinosaurs");
    for(const Dinosaur & d: dinos)
    {
        QDomElement dinoEl = DinoToXmlElement(doc, d);
        rootEl.appendChild(dinoEl);
    }
    doc.appendChild(rootEl);
    QString qstr=doc.toString(4);
    string str=qstr.toStdString();
    writefile(dinosaurs_file_, str);
    close();
}
int XmlStorage::getNewDinosaurId()
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    int max = 0;
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id > max)
        {
            max = dino.id;
        }
    }
    return max + 1;
}

Dinosaur xmlElementToDino(QDomElement & dinosaur)
{
    Dinosaur dinos;
    dinos.name=dinosaur.attribute("name").toStdString();
    dinos.length=dinosaur.attribute("length").toDouble();
    dinos.weight=dinosaur.attribute("weight").toDouble();
    dinos.order=dinosaur.attribute("order").toStdString();
    dinos.id=dinosaur.attribute("id").toInt();
    return dinos;
}
QDomElement DinoToXmlElement(QDomDocument & doc, const Dinosaur &dinosaur)
{
    QDomElement dinoEl=doc.createElement("dinosaur");
    dinoEl.setAttribute("id", dinosaur.id);
    dinoEl.setAttribute("name", QString::fromStdString(dinosaur.name));
    dinoEl.setAttribute("length", dinosaur.length);
    dinoEl.setAttribute("weight", dinosaur.weight);
    dinoEl.setAttribute("order", QString::fromStdString(dinosaur.order));
    return dinoEl;
}
