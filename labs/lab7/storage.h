#ifndef STORAGE_H
#define STORAGE_H

#include <string>
#include <vector>
#include <fstream>

#include <optional>
#include "fs.h"
#include "dino.h"
#pragma once
using namespace std;

class FileStorage
{
   string  dir_name_;

protected:
   fstream dinosaurs_file_;
   fstream eras_file_;

   virtual vector<Dinosaur> loadDinosaurs()=0;
   virtual void saveDinosaurs(const vector<Dinosaur> & dinosaurs)=0;
   virtual int getNewDinosaurId()=0;


 public:
   explicit FileStorage(const string & dir_name = "");
   virtual ~FileStorage() {}
   void setName(const string & dir_name);
   string name() const;

   bool isOpen() const;
   bool open();
   void close();

   vector<Dinosaur> getAllDinosaurs(void);
   optional<Dinosaur> getDinosaurById(int dinosaur_id);
   bool updateDinosaur(const Dinosaur &dinosaur);
   bool removeDinosaur(int dinosaur_id);
   int insertDinosaur(const Dinosaur &dinosaur);

};

#endif // STORAGE_H
