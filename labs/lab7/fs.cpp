#include "fs.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "fs.h"
using namespace std;

string readxmlfile (fstream & fin)
{
    if (!fin.is_open())
    {
        cerr << "file cannot be opened" << endl;
        exit(1);
    }
    string str;
    string s;
    while (!fin.eof())
    {
        getline(fin, s);
        int r=s.find('\r');
        if(r!=-1)
        {
            s.erase(r);
        }
        str += s;
    }
    fin.close();
    return str;
}
void writefile(fstream & fout, string & str)
{
    if (!fout.is_open())
    {
        cerr << "file cannot be opened" << endl;
        exit(1);
    }
    fout<<str;
    fout.close();
}
