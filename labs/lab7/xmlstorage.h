#ifndef XMLSTORAGE_H
#define XMLSTORAGE_H


#pragma once

#include "storage.h"
#include <QDebug>
#include <QtXml>
#include <QString>

using namespace std;

class XmlStorage: public FileStorage
{
   vector<Dinosaur> loadDinosaurs();
   void saveDinosaurs(const vector<Dinosaur> & dinosaurs);
   int getNewDinosaurId();


 public:
   explicit XmlStorage(const string & dir_name_ = "") : FileStorage(dir_name_) {}
};
#endif // XMLSTORAGE_H
