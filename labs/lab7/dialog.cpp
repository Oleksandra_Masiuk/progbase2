#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}
Dialog::~Dialog()
{
    delete ui;
}

Dinosaur Dialog::data()
{
    Dinosaur dino;
    dino.name = ui->lineEdit_name->text().toStdString();
    dino.order = ui->lineEdit_order->text().toStdString();
    dino.weight = ui->double_weight->value();
    dino.length = ui->double_length->value();
    return dino;

}
void Dialog::user_value(const Dinosaur &dino)
{
    ui->lineEdit_name->setText(QString::fromStdString(dino.name));
    ui->lineEdit_order->setText(QString::fromStdString(dino.order));
    ui->double_length->setValue(dino.length);
    ui->double_weight->setValue(dino.weight);
}



