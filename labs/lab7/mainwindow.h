#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "storage.h"
#include <QDebug>
#include <QFileDialog>
#include <QListWidgetItem>
#include "dialog.h"
#include "xmlstorage.h"
#include <QMessageBox>
#include <QDialog>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onNewStorage();
    void onOpenStorage();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void visibility();

    void on_Add_clicked();

    void on_Remove_clicked();

    void on_Edit_clicked();

private:
    Ui::MainWindow *ui;
    FileStorage * storage_;
    void dino_details(const Dinosaur & dino);
};

#endif // MAINWINDOW_H
