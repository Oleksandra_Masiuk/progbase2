#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "fs.h"
using namespace std;

StringTable readfile(fstream & fin)
{
    if (!fin.is_open())
    {
        cerr << "file cannot be opened" << endl;
        exit(1);
    }
    string str;
    string s;
    while (!fin.eof())
    {
        getline(fin, s);
        str += s;
        str += '\n';
    }
    size_t len = str.length() - 1;
    str[len] = '\0';
    StringTable t = Csv_parse(str);
    fin.close();
    return t;
}
string readxmlfile (fstream & fin)
{
    if (!fin.is_open())
    {
        cerr << "file cannot be opened" << endl;
        exit(1);
    }
    string str;
    string s;
    while (!fin.eof())
    {
        getline(fin, s);
        int r=s.find('\r');
        if(r!=-1)
        {
            s.erase(r);
        }
        str += s;
    }
    fin.close();
    return str;
}
void writefile(fstream & fout, string & csvtable)
{
    if (!fout.is_open())
    {
        cerr << "file cannot be opened" << endl;
        exit(1);
    }
    fout<<csvtable;
    fout.close();
}
