#include "csvstorage.h"
using namespace std;

vector<Dinosaur> CsvStorage:: loadDinosaurs()
{
    dinosaurs_file_.close();
    dinosaurs_file_.open(this->name() + "dinosaur.csv", ios::in);
    StringTable table = readfile(dinosaurs_file_);
    vector<Dinosaur> dinosaurs = createDinoListFromTable(table);
    return dinosaurs;
}
void CsvStorage:: saveDinosaurs(const vector<Dinosaur> & dinosaurs)
{
    dinosaurs_file_.close();
    dinosaurs_file_.open(this->name()+ "dinosaur.csv", ios::out);
    StringTable table = createTableFromList(dinosaurs, 5);
    string csvtable = Csv_toString(table);
    writefile(dinosaurs_file_, csvtable);
}
int CsvStorage:: getNewDinosaurId()
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    int max = 0;
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id > max)
        {
            max = dino.id;
        }
    }
    return max + 1;
}

// eras
vector<Era>CsvStorage:: loadEras()
{
    eras_file_.close();
    eras_file_.open(this->name() + "eras.csv", ios::in);
    StringTable table = readfile(eras_file_);
    vector<Era> eras=createEraListFromTable(table);
    return eras;
}
void CsvStorage::saveEras(const vector<Era> & eras)
{
    eras_file_.close();
    eras_file_.open(this->name() + "eras.csv", ios::out);
    StringTable table=createTableFromEraList(eras, 4);
    string csvtable = Csv_toString(table);
    writefile(eras_file_, csvtable);
}
int CsvStorage:: getNewEraSId()
{
    vector<Era> eras = this->loadEras();
    int max = 0;
    for (Era &er : eras)
    {
        if (er.id > max)
        {
            max = er.id;
        }
    }
    return max+1;
}
