#ifndef FS_H
#define FS_H

#include <stdio.h>
#include <stdlib.h>
#include "string_table.h"
#include "csv.h"
#include <iostream>
using namespace std;
#pragma once

StringTable readfile(fstream & fin);
void writefile(fstream & fout, string & csvtable);
string readxmlfile (fstream & fin);
#endif // FS_H
