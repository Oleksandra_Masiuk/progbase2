#include "cui.h"
#include <iostream>
#include <cstdio>
#include <QDebug>

void  Cui::dinosaursMainMenu()
{
    vector<Dinosaur> dino = storage_->getAllDinosaurs();
    for (Dinosaur &d : dino)
    {
        cout << d.id << " | " << d.name << endl;
    }
}
void  Cui::dinosaurMenu(int dinosaur_id)
{
    optional<Dinosaur> dopt = storage_->getDinosaurById(dinosaur_id);
    if (dopt)
    {
        Dinosaur &d = dopt.value();
        cout << d.id << " | " << d.name << " | "
             << "w=" << d.weight << " | "
             << " l= " << d.length << " | "
             << "order: " << d.order << endl;
    }
    else
    {
        cout << "wrong id try again" << endl;
    }
}
void  Cui::dinosaurUpdateMenu(int dinosaur_id)
{
    int counter=0;
    string menu;
    cout << "choose name, weight, length or order" << endl;
    getline(cin, menu);
    if (menu == "name" || menu == "weight" || menu == "length" || menu == "order")
    {
        bool wrong=false;
        string input_data;
        qDebug()<<"Enter data:";
        getline(cin, input_data);
        optional<Dinosaur> dopt = storage_->getDinosaurById(dinosaur_id);
        if (dopt)
        {
            Dinosaur &d=dopt.value();
            if (menu == "name" && menu.length()==4)
            {
                d.name=input_data;
                if (storage_->updateDinosaur(d))
                {
                    cout << "dinosaur updated" << endl;
                    dinosaurMenu(dinosaur_id);
                }
                else
                {
                    cout << "cannot update this dinosaur" << endl;
                }
            }
            else if (menu == "order" && menu.length()==5)
            {
                d.order=input_data;
                if (storage_->updateDinosaur(d))
                {
                    cout << "dinosaur updated" << endl;
                    dinosaurMenu(dinosaur_id);
                }
                else
                {
                    cout << "cannot update this dinosaur" << endl;
                }
            }
            else if (menu == "weight" && menu.length()==6)
            {
                wrong=false;
                for(int i=0; input_data[i]!='\0'; i++)
                {
                    if(!isdigit(input_data[i]))
                    {
                        qDebug()<<"weight or length can consist only numbers";
                        wrong=true;
                        break;
                    }
                }
                if(wrong==false)
                {
                    d.weight=stoi(input_data);
                    if (storage_->updateDinosaur(d))
                    {
                        cout << "dinosaur updated" << endl;
                        dinosaurMenu(dinosaur_id);
                    }
                    else
                    {
                        cout << "cannot update this dinosaur" << endl;
                    }
                }
            }
            else if (menu == "length" && menu.length()==6 && wrong==false)
            {
                wrong=false;
                for(int i=0; input_data[i]!='\0'; i++)
                {
                    if(!isdigit(input_data[i]))
                    {
                        qDebug()<<"weight or length can consist only numbers";
                        wrong=true;
                        break;
                    }
                }
                if(wrong==false)
                {
                d.length=stoi(input_data);
                if (storage_->updateDinosaur(d))
                {
                    cout << "dinosaur updated" << endl;
                    dinosaurMenu(dinosaur_id);
                }
                else
                {
                    cout << "cannot update this dinosaur" << endl;
                }
                }
            }

        }
        else
        {
            cout << "wrong id, try again" << endl;
        }
    }
    else
    {
        qDebug() << "wrong option try again";
    }
}
void  Cui::dinosaurDeleteMenu(int dinosaur_id)
{
    bool status = storage_->removeDinosaur(dinosaur_id);
    if (status)
    {
        cout << "removed" << endl;
    }
    else
    {
        cout << "cannot remove" << endl;
    }
}
void  Cui::dinosaurCreateMenu()
{
    bool wrong=false;
    string name;
    string weight;
    string order;
    string length;
    cout << "please, enter name, weight, length and order" << endl;
    getline(cin,name);
    getline(cin,weight);
    getline(cin,length);
    getline(cin,order);
    for(int i=0; length[i]!='\0'; i++)
    {
        if(!isdigit(length[i]))
        {
            qDebug()<<"weight or length can consist only numbers";
            wrong=true;
            break;
        }
    }
    for(int i=0; weight[i]!='\0'; i++)
    {
        if(!isdigit(weight[i]))
        {
            qDebug()<<"weight or length can consist only numbers";
            wrong=true;
            break;
        }
    }
    if(wrong==false)
    {
        Dinosaur d{name, stoi(weight), stoi(length), order, -1};
        int newId = storage_->insertDinosaur(d);
        d.id = newId;
        dinosaursMainMenu();
    }
}
void  Cui::task1()
{
    bool wrong=false;
    string str_command;
    int command=0;
    qDebug()<<"Enter command:";
    getline(cin, str_command);
    for(int i=0; str_command[i]!='\0';i++)
    {
        if(!isdigit(str_command[i]))
        {
            qDebug()<<"command can consist only letters";
            wrong=true;
            break;
        }
    }
    if(wrong==false)
    {
        command=stoi(str_command);
        if(command==1)
        {
            dinosaurCreateMenu();
        }
        else if(command==2)
        {
            bool w=false;
            string str_id;
            qDebug()<<"enter id";
            getline(cin, str_id);
            for(int i=0; str_id[i]!='\0';i++)
            {
                if(!isdigit(str_id[i]))
                {
                    qDebug()<<"command can consist only letters";
                    w=true;
                    break;
                }
            }
            if(w==false)
            {
                dinosaurDeleteMenu(stoi(str_id));
            }

        }
        else if(command==3)
        {
            bool w=false;
            qDebug()<<"enter id";
            string str_id;
            getline(cin, str_id);
            for(int i=0; str_id[i]!='\0';i++)
            {
                if(!isdigit(str_id[i]))
                {
                    qDebug()<<"command can consist only letters";
                    w=true;
                    break;
                }
            }
            if(w==false)
            {
                dinosaurUpdateMenu(stoi(str_id));
            }

        }
        else if(command==4)
        {
            dinosaursMainMenu();
        }
        else if(command==5)
        {
            bool w=false;
            qDebug()<<"enter id";
            string str_id;
            getline(cin, str_id);
            for(int i=0; str_id[i]!='\0';i++)
            {
                if(!isdigit(str_id[i]))
                {
                    qDebug()<<"command can consist only letters";
                    w=true;
                    break;
                }
            }
            if(w==false)
            {
                dinosaurMenu(stoi(str_id));
            }

        }
        else
        {
            qDebug()<<"wrong command";
        }
    }
}

void  Cui::eraMainMenu()
{
    vector<Era> era = storage_->getAllEras();
    for (Era &e : era)
    {
        cout << e.id << " | " << e.name << endl;
    }
}
void  Cui::eraMenu(int era_id)
{
    optional<Era> eopt = storage_->getEraById(era_id);
    if (eopt)
    {
        Era &e = eopt.value();
        cout << e.id << " | " << e.name << " | " << e.from << " | " << e.to << endl;
    }
    else
    {
        cout << "wrong id try again" << endl;
    }
}
void  Cui::eraUpdateMenu(int era_id)
{
    int counter=0;
    string menu;
    cout << "choose name, from or to" << endl;
    getline(cin, menu);
    if (menu == "name" || menu == "from" || menu == "to")
    {
        bool wrong=false;
        string input_data;
        cout<<"Enter data:"<<endl;
        getline(cin, input_data);
        optional<Era> eopt = storage_->getEraById(era_id);
        if (eopt)
        {
            Era &e=eopt.value();
            if (menu == "name" && menu.length()==4)
            {
                e.name=input_data;
                if (storage_->updateEra(e))
                {
                    cout << "era updated" << endl;
                    eraMenu(era_id);
                }
                else
                {
                    cout << "cannot update this era" << endl;
                }
            }
            else if (menu == "from" && menu.length()==4 && wrong==false)
            {
                wrong=false;
                for(int i=0; input_data[i]!='\0'; i++)
                {
                    if(!isdigit(input_data[i]))
                    {
                        qDebug()<<"from or to can consist only numbers";
                        wrong=true;
                        break;
                    }
                }
                if(wrong==false)
                {
                e.from =stoi(input_data);
                if (storage_->updateEra(e))
                {
                    cout << "era updated" << endl;
                    eraMenu(era_id);
                }
                else
                {
                    cout << "cannot update this era" << endl;
                }
                }
            }
            else if (menu == "to" && menu.length()==2 && wrong==false)
            {
                wrong=false;
                for(int i=0; input_data[i]!='\0'; i++)
                {
                    if(!isdigit(input_data[i]))
                    {
                        qDebug()<<"from or to can consist only numbers";
                        wrong=true;
                        break;
                    }
                }
                if(wrong==false)
                {
                e.to=stoi(input_data);
                if (storage_->updateEra(e))
                {
                    cout << "era updated" << endl;
                    eraMenu(era_id);
                }
                else
                {
                    cout << "cannot update this era" << endl;
                }
                }
            }

        }
        else
        {
            cout << "wrong id, try again" << endl;
        }
    }
    else
    {
        qDebug() << "wrong option try again";
    }
}
void  Cui::eraDeleteMenu(int era_id)
{
    bool status = storage_->removeEra(era_id);
    if (status)
    {
        cout << "removed" << endl;
    }
    else
    {
        cout << "cannot remove" << endl;
    }
}
void  Cui::eraCreateMenu()
{
    bool wrong=false;
    string from;
    string to;
    string name;
    cout << "please, enter name" << endl;
    getline(cin, name);
    cout << "please, enter from " << endl;
    getline(cin, from);
    cout << "please, enter to" << endl;
    getline(cin,to);
    for(int i = 0; i < from.length(); i++)
    {
        if(!isdigit(from[i]))
        {
            qDebug()<<"weight or length can consist only numbers";
            wrong=true;
            break;
        }
    }
    for(int i = 0; i < to.length(); i++)
    {
        if(!isdigit(to[i]))
        {
            qDebug()<<"weight or length can consist only numbers";
            wrong=true;
            break;
        }
    }
    if(wrong==false)
    {
        Era e{name, stoi(from), stoi(to), 0};
        int newId = storage_->insertEra(e);
        e.id = newId;
        eraMainMenu();
    }
}
void  Cui::task2()
{
    bool wrong=false;
    string str_command;
    int command=0;
    qDebug()<<"Enter command:";
    getline(cin, str_command);
    for(int i=0; str_command[i]!='\0';i++)
    {
        if(!isdigit(str_command[i]))
        {
            qDebug()<<"command can consist only letters";
            wrong=true;
            break;

        }
    }
    if(wrong==false)
    {
        command=stoi(str_command);
        if(command==1)
        {
            eraCreateMenu();
        }
        else if(command==2)
        {
            bool w=false;
            string str_id;
            qDebug()<<"enter id";
            getline(cin, str_id);
            for(int i=0; str_id[i]!='\0';i++)
            {
                if(!isdigit(str_id[i]))
                {
                    qDebug()<<"command can consist only letters";
                    w=true;
                    break;
                }
            }
            if(w==false)
            {
                eraDeleteMenu(stoi(str_id));
            }

        }
        else if(command==3)
        {
            bool w=false;
            string str_id;
            qDebug()<<"enter id";
            getline(cin, str_id);
            for(int i=0; str_id[i]!='\0';i++)
            {
                if(!isdigit(str_id[i]))
                {
                    qDebug()<<"command can consist only letters";
                    w=true;
                    break;
                }
            }
            if(w==false)
            {
                eraUpdateMenu(stoi(str_id));
            }

        }
        else if(command==4)
        {
            eraMainMenu();
        }
        else if(command==5)
        {
            bool w=false;
            string str_id;
            qDebug()<<"enter id";
            getline(cin, str_id);
            for(int i=0; str_id[i]!='\0';i++)
            {
                if(!isdigit(str_id[i]))
                {
                    qDebug()<<"command can consist only numbers";
                    w=true;
                    break;
                }
            }
            if(w==false)
            {
                eraMenu(stoi(str_id));
            }

        }
        else
        {
            qDebug()<<"wrong command";
        }
    }
}

enum Commands
{
    Exit,
    Create,
    Delete,
    Update,
    GetAll,
    GetByID,
    MaxCommand
};

void Cui::show()
{
    int option=-1;
    while (option!=3)
    {
        qDebug()<<"Enter 1 to work with dinosaurs, 2 to work with eras, 3 to exit";
        bool w=false;
        string str_opt;
        getline(cin, str_opt);
        for(int i=0; str_opt[i]!='\0';i++)
        {
            if(!isdigit(str_opt[i]))
            {
                qDebug()<<"command can consist only numbers";
                w=true;
                break;
            }
        }
        if(w==false)
        {
            option=(stof(str_opt));
            if(option==1)
            {
                qDebug()<<"1) Create";
                qDebug()<<"2) Delete";
                qDebug()<<"3) Update";
                qDebug()<<"4) Show all";
                qDebug()<<"5) Show by id";
                task1();
            }
            else if(option==2)
            {
                qDebug()<<"1) Create";
                qDebug()<<"2) Delete";
                qDebug()<<"3) Update";
                qDebug()<<"4) Show all";
                qDebug()<<"5) Show by id";
                task2();
            }
            else if(option!=1 && option!=2 && option!=3)
            {
                qDebug()<<"Try again";
            }
        }

    }
}

