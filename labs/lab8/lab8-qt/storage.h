#ifndef STORAGE_H
#define STORAGE_H
#pragma once

#include <string>
#include <vector>

#include<optional>
#include "dino.h"
#include "era.h"

using namespace std;

class Storage
{
    string dir_name_;
public:
    Storage();
    explicit Storage(const string & dir_name);
    virtual ~Storage() {}

    void setName(const string & dir_name);
    string name() const;

    virtual bool isOpen() const = 0;
    virtual bool open() = 0;
    virtual void close() = 0;

    virtual vector<Dinosaur> getAllDinosaurs(void)=0;
    virtual optional<Dinosaur> getDinosaurById(int dinosaur_id)=0;
    virtual bool updateDinosaur(const Dinosaur &dinosaur)=0;
    virtual bool removeDinosaur(int dinosaur_id)=0;
    virtual int insertDinosaur(const Dinosaur &dinosaur)=0;

    virtual vector<Era> getAllEras(void)=0;
    virtual optional<Era> getEraById(int era_id)=0;
    virtual bool updateEra(const Era &era)=0;
    virtual bool removeEra(int era_id)=0;
    virtual int insertEra(const Era &era)=0;


};

#endif // STORAGE_H
