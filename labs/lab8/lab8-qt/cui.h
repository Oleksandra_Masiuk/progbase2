#ifndef CUI_H
#define CUI_H

using namespace std;
#include <iostream>
#include "file_storage.h"
#include "sqlite_storage.h"
#include "dino.h"
#include <optional>
#include <vector>
#pragma once
class Cui
{
    Storage * const storage_;
    void dinosaursMainMenu();
    void dinosaurMenu(int dinosaur_id);
    void dinosaurUpdateMenu(int dinosaur_id);
    void dinosaurDeleteMenu(int dinosaur_id);
    void dinosaurCreateMenu();
    void task1();

    void eraMainMenu();
    void eraMenu(int era_id);
    void eraUpdateMenu(int era_id);
    void eraDeleteMenu(int era_id);
    void eraCreateMenu();
    void task2();
public:
    Cui(Storage * storage): storage_{storage} {}
    //
    void show();
};

#endif // CUI_H






