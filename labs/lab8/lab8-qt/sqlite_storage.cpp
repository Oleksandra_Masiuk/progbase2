#include "sqlite_storage.h"
#include <QSqlQuery>
#include <QtSql>
#include <QDebug>

SqliteStorage::SqliteStorage(const string &dir_name):Storage(dir_name)
{
    db_=QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::isOpen() const
{
    return db_.isOpen();
}
bool SqliteStorage::open()
{
    QString path=QString::fromStdString(this->name())+"/data.sqlite";
    db_.setDatabaseName(path);
    bool connected =db_.open();
    if(!connected)
    {
        return false;
    }
    return true;
}
void SqliteStorage::close()
{
    db_.close();
}

Dinosaur getDinosaurFromQuerry(const QSqlQuery & query)

{
    int id=query.value("id").toInt();
    string name=query.value("name").toString().toStdString();
    int length=query.value("length_dino").toInt();
    int weight=query.value("weight").toInt();
    string order = query.value("order_dino").toString().toStdString();
    Dinosaur dino;
    dino.id=id;
    dino.name=name;
    dino.length=length;
    dino.weight=weight;
    dino.order=order;
    return dino;
}
vector<Dinosaur> SqliteStorage::getAllDinosaurs(void)
{
    QSqlQuery query("SELECT * FROM dinosaurs");
    vector<Dinosaur> dinosaurs;
    while (query.next())
    {
       Dinosaur dino=getDinosaurFromQuerry(query);
       dinosaurs.push_back(dino);
    }
    return dinosaurs;
}
optional<Dinosaur> SqliteStorage::getDinosaurById(int dinosaur_id)
{
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM dinosaurs WHERE id = :id"))
    {
       qDebug() << "get dinosaur query prepare error:" << query.lastError();
       return nullopt;
    }
    query.bindValue(":id", dinosaur_id);
    if (!query.exec())
    {  // do exec if query is prepared SELECT query
       qDebug() << "get dinosaur query exec error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
       Dinosaur dino=getDinosaurFromQuerry(query);
       return dino;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }
}
bool SqliteStorage::updateDinosaur(const Dinosaur &dinosaur)
{
    QSqlQuery query;
    query.prepare("UPDATE dinosaurs SET name = :name, length_dino = :length_dino, weight = :weight, order_dino = :order_dino WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(dinosaur.name));
    query.bindValue(":length_dino", dinosaur.length);
    query.bindValue(":weight", dinosaur.weight);
    query.bindValue(":order_dino",  QString::fromStdString(dinosaur.order));
    query.bindValue(":id", dinosaur.id);
    if (!query.exec())
    {
        qDebug() << "updateDinosaur query exec error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
bool SqliteStorage::removeDinosaur(int dinosaur_id)
{
    QSqlQuery query;
    if (!query.prepare("DELETE FROM dinosaurs WHERE id = :id"))
    {
        qDebug() << "deleteDino query prepare error:" << query.lastError();
        return false;
    }
    query.bindValue(":id", dinosaur_id);
    if (!query.exec())
    {
        qDebug() << "deleteDino query exec error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertDinosaur(const Dinosaur &dinosaur)
{
    QSqlQuery query;
    query.prepare("INSERT INTO dinosaurs (name, length_dino, weight, order_dino) VALUES (:name, :length_dino, :weight, :order_dino)");
         QSqlError error = query.lastError();
    query.bindValue(":name", QString::fromStdString(dinosaur.name));
    query.bindValue(":length_dino", dinosaur.length);
    query.bindValue(":weight", dinosaur.weight);
    query.bindValue(":order_dino", QString::fromStdString(dinosaur.order));
    if (!query.exec())
    {
       qDebug() << "addDino query exec error:"
                << query.lastError();
       return 0;
    }
    QVariant var= query.lastInsertId();
    return var.toInt();
}
Era getEraFromQuerry(const QSqlQuery & query)
{
    int id=query.value("id").toInt();
    string name=query.value("name").toString().toStdString();
    int from=query.value("from_year").toInt();
    int to=query.value("to_year").toInt();
    Era era;
    era.id=id;
    era.name=name;
    era.from=from;
    era.to=to;
    return era;
}
vector<Era> SqliteStorage::getAllEras(void)
{
    QSqlQuery query("SELECT * FROM eras");
    vector<Era> eras;
    while (query.next())
    {
       Era era=getEraFromQuerry(query);
       eras.push_back(era);
    }
    return eras;
}
optional<Era> SqliteStorage::getEraById(int era_id)
{
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM eras WHERE id = :id"))
    {
       qDebug() << "get era query prepare error:" << query.lastError();
       return nullopt;
    }
    query.bindValue(":id", era_id);
    if (!query.exec())
    {  // do exec if query is prepared SELECT query
       qDebug() << "get era query exec error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
       Era era=getEraFromQuerry(query);
       return era;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }
}
bool SqliteStorage::updateEra(const Era &era)
{
    QSqlQuery query;
    if (!query.prepare("UPDATE eras SET name = :name, from_year = :from_year, to_year = :to_year WHERE id = :id"))
    {
        qDebug() << "updateEra query prepare error:" << query.lastError();
        return false;
    }
    query.bindValue(":name", QString::fromStdString(era.name));
    query.bindValue(":from_year", era.from);
    query.bindValue(":to_year", era.to);
    query.bindValue(":id", era.id);
    if (!query.exec())
    {
        qDebug() << "updateEra query exec error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
bool SqliteStorage::removeEra(int era_id)
{
    QSqlQuery query;
    if (!query.prepare("DELETE FROM eras WHERE id = :id"))
    {
        qDebug() << "deleteEra query prepare error:" << query.lastError();
        return false;
    }
    query.bindValue(":id", era_id);
    if (!query.exec())
    {
        qDebug() << "deleteEra query exec error:" << query.lastError();
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
int SqliteStorage::insertEra(const Era &era)
{
    QSqlQuery query;
    query.prepare("INSERT INTO eras (name, from_year, to_year) VALUES (:name, :from_year, :to_year)");
    query.bindValue(":name", QString::fromStdString(era.name));
    query.bindValue(":from_year", era.from);
    query.bindValue(":to_year", era.to);
    if (!query.exec())
    {
       qDebug() << "addEra query exec error:"
                << query.lastError();
       return 0;
    }
    QVariant var= query.lastInsertId();
    return var.toInt();
}
