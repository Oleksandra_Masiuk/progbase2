#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <optional>
#include <vector>

#include "sqlite_storage.h"
#include "cui.h"
#include "csvstorage.h"
#include "xmlstorage.h"
using namespace std;

int main()
{
    cout<<"we are workng with Sql file"<<endl;
    SqliteStorage sql_storage{"./../data/sql"};
    Storage * storage_ptr=&sql_storage;
    storage_ptr->open();
    Cui cui(storage_ptr);
    cui.show();
    storage_ptr->close();
    return 0;
}
