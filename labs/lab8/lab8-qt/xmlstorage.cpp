#include "xmlstorage.h"
#include "fs.h"
#include <QDebug>
#include <QtXml>
#include <QString>
using namespace std;
Dinosaur xmlElementToDino(QDomElement & dinosaur);
QDomElement DinoToXmlElement(QDomDocument & doc, const Dinosaur &dinosaur);

vector<Dinosaur> XmlStorage:: loadDinosaurs()
{
    dinosaurs_file_.close();
    dinosaurs_file_.open(this->name() + "dinosaur.xml", ios::in);
    vector<Dinosaur> dinosaurs;
    string str=readxmlfile(dinosaurs_file_);
    QString xmlText=QString::fromStdString(str);
    vector<Dinosaur> dino;
    QDomDocument doc;
    QString errorMessage;
    if (!doc.setContent(xmlText, &errorMessage))
    {
        qDebug() << "error parsing xml: " << errorMessage;
        exit(1);
    }
    QDomElement rootEl = doc.documentElement();
    QDomNodeList rootElChildren = rootEl.childNodes();
    for(int i=0; i<rootElChildren.length(); i++)
    {
        QDomNode dinoNode=rootElChildren.at(i);
        QDomElement dinoEl =dinoNode.toElement();
        Dinosaur d=xmlElementToDino(dinoEl);
        dino.push_back(d);
    }
    return dino;

}
void XmlStorage:: saveDinosaurs(const vector<Dinosaur> & dinosaurs)
{
    dinosaurs_file_.close();
    dinosaurs_file_.open(this->name()+ "dinosaur.xml", ios::out);
    vector<Dinosaur> dinos=dinosaurs;
    QDomDocument doc;
    QDomElement rootEl = doc.createElement("dinosaurs");
    for(const Dinosaur & d: dinos)
    {
        QDomElement dinoEl = DinoToXmlElement(doc, d);
        rootEl.appendChild(dinoEl);
    }
    doc.appendChild(rootEl);
    QString qstr=doc.toString(4);
    string str=qstr.toStdString();
    writefile(dinosaurs_file_, str);
}
int XmlStorage::getNewDinosaurId()
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    int max = 0;
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id > max)
        {
            max = dino.id;
        }
    }
    return max + 1;
}
QDomElement eraToXmlElement(QDomDocument & doc, const Era &era);
Era xmlElementToEra(QDomElement & era);
vector<Era> XmlStorage::loadEras()
{
    eras_file_.close();
    eras_file_.open(this->name() + "eras.xml", ios::in);
    vector<Era> eras;
    string str=readxmlfile(eras_file_);
    QString xmlText=QString::fromStdString(str);
    QDomDocument doc;
    QString errorMessage;
    if (!doc.setContent(xmlText, &errorMessage))
    {
        qDebug() << "error parsing xml: " << errorMessage;
        exit(1);
    }
    QDomElement rootEl = doc.documentElement();
    QDomNodeList rootElChildren = rootEl.childNodes();
    for(int i = 0; i < rootElChildren.length(); i++)
    {
        QDomNode eraNode=rootElChildren.at(i);
        QDomElement eraEl=eraNode.toElement();
        Era e=xmlElementToEra(eraEl);
        eras.push_back(e);
    }
    return eras;
}
void XmlStorage::saveEras(const vector<Era> & eras)
{
    eras_file_.close();
    eras_file_.open(this->name() + "eras.xml", ios::out);
    vector<Era> era=eras;
    QDomDocument doc;
    QDomElement rootEl=doc.createElement("eras");
    for(const Era &er :era)
    {
        QDomElement eraEl=eraToXmlElement(doc, er);
        rootEl.appendChild(eraEl);
    }
    doc.appendChild(rootEl);
    QString qstr=doc.toString(4);
    string str=qstr.toStdString();
    writefile(eras_file_, str);

}
int XmlStorage::getNewEraSId()
{
    vector<Era> eras = this->loadEras();
    int max = 0;
    for (Era &er : eras)
    {
        if (er.id > max)
        {
            max = er.id;
        }
    }
    return max+1;
}
Dinosaur xmlElementToDino(QDomElement & dinosaur)
{
    Dinosaur dinos;
    dinos.name=dinosaur.attribute("name").toStdString();
    dinos.length=dinosaur.attribute("length").toDouble();
    dinos.weight=dinosaur.attribute("weight").toDouble();
    dinos.order=dinosaur.attribute("order").toStdString();
    dinos.id=dinosaur.attribute("id").toInt();
    return dinos;
}
QDomElement DinoToXmlElement(QDomDocument & doc, const Dinosaur &dinosaur)
{
    QDomElement dinoEl=doc.createElement("dinosaur");
    dinoEl.setAttribute("id", dinosaur.id);
    dinoEl.setAttribute("name", QString::fromStdString(dinosaur.name));
    dinoEl.setAttribute("length", dinosaur.length);
    dinoEl.setAttribute("weight", dinosaur.weight);
    dinoEl.setAttribute("order", QString::fromStdString(dinosaur.order));
    return dinoEl;
}
QDomElement eraToXmlElement(QDomDocument & doc, const Era &era)
{
    QDomElement eras=doc.createElement("era");
    eras.setAttribute("id", era.id);
    eras.setAttribute("name", QString::fromStdString(era.name));
    eras.setAttribute("from", era.from);
    eras.setAttribute("to", era.to);
    return eras;
}
Era xmlElementToEra(QDomElement & era)
{
    Era eras;
    eras.id=era.attribute("id").toInt();
    eras.name=era.attribute("name").toStdString();
    eras.from=era.attribute("from").toInt();
    eras.to=era.attribute("to").toInt();
    return eras;
}
