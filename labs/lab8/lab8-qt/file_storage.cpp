#include "file_storage.h"

using namespace std;


bool FileStorage::isOpen() const
{
    return (dinosaurs_file_.is_open()&eras_file_.is_open());
}
bool FileStorage::open()
{
    size_t startlength = name().length() - 4;
    size_t endlength = name().length() - 1;
    if (name().substr(startlength, endlength) != "/csv" && name().substr(startlength, endlength) != "/xml")
    {
       cout << "Error, incorrect path." << endl;
       exit(1);

    }
    if (name().substr(startlength, endlength) == "/csv")
    {
       dinosaurs_file_.open(name() + "/dinosaur.csv");
       eras_file_.open(name() + "/eras.csv");

    }
    if(name().substr(startlength, endlength) == "/xml")
    {
       dinosaurs_file_.open(name() + "/dinosaur.xml");
       eras_file_.open(name() + "/eras.xml");

    }
    return isOpen();
}
void FileStorage::close()
{
    dinosaurs_file_.close();
    eras_file_.close();
}

vector<Dinosaur> FileStorage::getAllDinosaurs(void)
{
    return this->loadDinosaurs();
}
vector<Era> FileStorage::getAllEras(void)
{
    return this->loadEras();
}
optional<Dinosaur> FileStorage::getDinosaurById(int dinosaur_id)
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id == dinosaur_id)
        {
            return dino;
        }
    }
    return nullopt;
}
optional<Era> FileStorage::getEraById(int era_id)
{
    vector<Era> eras = this->loadEras();
    for (Era &era : eras)
    {
        if (era.id == era_id)
        {
            return era;
        }
    }
}

bool FileStorage::updateDinosaur(const Dinosaur &dinosaur)
{
    int dinosaur_id = dinosaur.id;
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id == dinosaur_id)
        {
            dino = dinosaur;
            this->saveDinosaurs(dinosaurs);
            return true;
        }
    }
    return false;
}
bool FileStorage::updateEra(const Era &era)
{
    int era_id = era.id;
    vector<Era> eras = this->loadEras();
    for (Era &er : eras)
    {
        if (er.id == era_id)
        {
            er = era;
            this->saveEras(eras);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeDinosaur(int dinosaur_id)
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    auto it = dinosaurs.begin();
    auto it_end = dinosaurs.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == dinosaur_id)
        {
            dinosaurs.erase(it);
            this->saveDinosaurs(dinosaurs);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeEra(int era_id)
{
    vector<Era> eras = this->loadEras();
    auto it = eras.begin();
    auto it_end = eras.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == era_id)
        {
            eras.erase(it);
            this->saveEras(eras);
            return true;
        }
    }
    return false;
}
int FileStorage::insertDinosaur(const Dinosaur &dinosaur)
{
    int newId = this->getNewDinosaurId();
    Dinosaur copy = dinosaur;
    copy.id = newId;
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    dinosaurs.push_back(copy);
    this->saveDinosaurs(dinosaurs);
    return newId;
}
int FileStorage::insertEra(const Era &era)
{
    int newId = this->getNewEraSId();
    Era copy = era;
    copy.id = newId;
    vector<Era> eras = this->loadEras();
    eras.push_back(copy);
    this->saveEras(eras);
    return newId;
}






