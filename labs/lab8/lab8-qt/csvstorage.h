#ifndef CSVSTORAGE_H
#define CSVSTORAGE_H



#pragma once

#include "file_storage.h"
#include "string_table.h"
#include "dino.h"
#include "fs.h"
#include "era.h"
#include "list.h"
#pragma once

using namespace std;

class CsvStorage: public FileStorage
{
   // dinos
   vector<Dinosaur> loadDinosaurs();
   void saveDinosaurs(const vector<Dinosaur> & dinosaurs);
   int getNewDinosaurId();

   // eras
   vector<Era> loadEras();
   void saveEras(const vector<Era> & eras);
   int getNewEraSId();

 public:
   explicit CsvStorage(const string & dir_name_ = "") : FileStorage(dir_name_) {}
};


#endif // CSVSTORAGE_H
