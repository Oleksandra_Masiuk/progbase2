QT += sql
QT += xml
QT -= gui

CONFIG += c++1z console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    sqlite_storage.cpp \
    main.cpp \
    storage.cpp \
    file_storage.cpp \
    cui.cpp \
    fs.cpp \
    xmlstorage.cpp \
    string_table.cpp \
    csv.cpp \
    list.cpp \
    csvstorage.cpp

HEADERS += \
    era.h \
    dino.h \
    dino.h \
    era.h \
    sqlite_storage.h \
    storage.h \
    file_storage.h \
    cui.h \
    fs.h \
    xmlstorage.h \
    string_table.h \
    csv.h \
    list.h \
    csvstorage.h
