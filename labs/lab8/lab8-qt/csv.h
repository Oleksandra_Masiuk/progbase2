#ifndef LIBCSVLAB_H
#define LIBCSVLAB_H
#include "string_table.h"
#pragma once
using namespace std;
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdio.h>

StringTable Csv_parse(string &csvStr);

string Csv_toString(StringTable &table);

#endif // LIBCSVLAB_H
