#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dynamic_array.h"
#pragma once
class List
{
    DynamicArray array_; // dynamic array of T elements
    size_t size_;

public:
    List();
    ~List();

    size_t size(); // return number of items in list

    double get(int index);                // return self->items[index]
    void set(int index, double value);    // set self->items[index]
    void insert(int index, double value); // insert, shift right
    void remove_at(int index);       // remove and shift left

    void push_back(double value); // insert back
    void remove(double value);    // remove first by value
    int index_of(double value);   // find index by value
    bool contains(double value);  // check by value
    bool empty();            // check if list has any items
    void clear();            // make list empty
    void print ();
};
