#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#pragma once
class DynamicArray
{
  double * _items;
  size_t _capacity;

public:
  DynamicArray();
  ~DynamicArray();

  size_t size();
  void resize();

  double get(int index);
  void set(int index, double value);
  void print(int length);
  void printback(int length);
};

