#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dynamic_array.h"
DynamicArray::DynamicArray()
{
    //std::cout << "dynarray created" << std::endl;
    _capacity=16;
    _items=new double[_capacity];
}
DynamicArray::~DynamicArray()
{
    //std::cout << "dynarray deleted" << std::endl;
    delete[] _items;
}
size_t DynamicArray::size()
{
    return _capacity;
}
void DynamicArray::print(int length)
{
    std::cout << "\n" << "dynarray:" <<  std::endl;
    for(int i=0; i<length;i++)
    {
        std::cout << " " <<_items[i]  << " " ;
    }    
}
void DynamicArray::printback(int length)
{
    std::cout << "\n" << "dynarray:" <<  std::endl;
    for(int i=length-1; i>=0;i--)
    {
        std::cout << " " <<_items[i]  << " " ;
    }    
}
void DynamicArray::resize()
{
    double * _items2;
    _items2=new double[_capacity*2];
    for (int i=0; (int)i<_capacity;i++)
    {
        _items2[i]=_items[i];
    }
    _capacity=_capacity*2;
    delete _items;
    _items=_items2;
}
double DynamicArray::get(int index)
{
    if (index>=_capacity)
    {
        std::cerr << "wrong index" << std::endl;
    }
    else
    {
        return _items[index];
    }
}
void DynamicArray::set(int index, double value)
{
    if (index>=_capacity)
    {
        DynamicArray::resize();
    }
    _items[index]=value;
}