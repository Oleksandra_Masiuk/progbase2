#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dynamic_array.h"
#include "deque.h"
Deque::Deque()
{
    DynamicArray array;
    size_ = 0;
}
Deque::~Deque()
{
    size_ = 0;
}
size_t Deque::size()
{
    return size_;
}
void Deque::push_back(double value)
{
    if (size_ == array.size())
    {
        array.resize();
    }
    array.set(size_, value);
    size_++;
}
double Deque::pop_back()
{
    if (size_ > 0)
    {
        size_--;
    }
}
void Deque::push_front(double value)
{
    if (size_ == array.size())
    {
        array.resize();
    }
    int index = 0;
    for (int i = size_; i > index; i--)
    {
        array.set(i, array.get(i - 1));
    }
    array.set(index, value);
    size_ = size_ + 1;
}
double Deque::pop_front()
{
    if (size_ > 0)
    {
        for (int i = 0; i < size_ - 1; i++)
        {
            array.set(i, array.get(i + 1));
        }
        size_--;
    }
}
bool Deque::empty()
{
    return (size_ == 0);
}
void Deque::print(int length)
{
    array.print(size_);
}
void Deque::printback(int length)
{
    array.printback(size_);
}
double Deque::get(int index)
{
    if (index < size_)
    {
        return array.get(index);
    }
    else
    {
        std::cerr << "wrong index" << std::endl;
    }
}