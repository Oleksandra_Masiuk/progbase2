#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dynamic_array.h"
#pragma once
class Deque
{
   DynamicArray array;
   size_t size_;
public:
   Deque();
   ~Deque();
   size_t size(); // return number of items

   void push_back(double value);
   double pop_back();
   void push_front(double value);
   double pop_front();
   void print(int length);
   void printback(int length);
   double get (int index);

   bool empty();
};
