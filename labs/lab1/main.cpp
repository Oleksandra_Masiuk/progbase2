#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>
#include <ctype.h>
#include <string.h>
#include <progbase.h>
#include "fs.h"
#include "dyn_list.h"
#include "deque.h"
int main()
{
    int i=0;
    List l1;
    openfile(l1);
    std::cout << "\n" << "first list";
    l1.print();
    size_t len=l1.size();
    for (int i=0; i<len;i++)
    {
        if (fabs(l1.get(i))<1)
        {
            l1.push_back(l1.get(i));
        }
    }
    while (l1.size()>len)
    {
        if (fabs(l1.get(i))<1)
        {
            l1.remove_at(i);
        }
        else
        {
            i++;
        }
    }
    std::cout << "\n" << "changed first list";
    l1.print();
    Deque d1;
    Deque d2;
    for(int i=0; i<l1.size(); i++)
    {
        if (i%2!=0)
        {
            d1.push_front(l1.get(i));
        }
        else
        {
            d2.push_back(l1.get(i));
        }
    }
    std::cout << "\n" << "first deque";
    d1.print(d1.size());
    std::cout << "\n" << "second deque";
    d2.printback(d2.size());
    List l2;
    for (int i=d1.size()-1; i>=0; i--)
    {
        l2.push_back(d1.get(i));
    }
    for (int i=d2.size()-1; i>=0; i--)
    {
        l2.push_back(d2.get(i));
    }
    std::cout << "\n" << "second list";
    l2.print();
}