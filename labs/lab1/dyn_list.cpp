#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dynamic_array.h"
#include <math.h>
#include "dyn_list.h"
List::List()
{
    DynamicArray array_; // dynamic array of T elements
    size_=0;
}
List::~List()
{
    size_=0;
}
void List::print()
{
    array_.print(size_);
}
size_t List::size()
{
    return size_;
}
double List::get(int index)
{
    if (index < size_)
    {
        return array_.get(index);
    }
    else
    {
        std::cerr << "wrong index" << std::endl;
    }
}
void List::set(int index, double value)
{
    if (index < size_)
    {
        array_.set(index, value);
    }
    else
    {
        std::cerr << "wrong index" << std::endl;
    }
}
void List::insert(int index, double value)
{
    size_t cap=array_.size();
    if(size_==cap)
    {
        array_.resize();
    }
    if (index < size_ && index>0)
    {
        for (int i=size_; i>index; i--)
        {
            array_.set(i, array_.get(i-1));
        }
        array_.set(index, value);
        size_=size_+1;
    }
    else
    {
        std::cerr << "wrong index" << std::endl;
    }
}
void List::remove_at(int index)
{
    if (index<0)
    {
        std::cerr << "wrong index" << std::endl;
    }
    else
    {
        for (int i=index;  i<size_-1; i++)
        {
            array_.set(i, array_.get(i+1));
        }
        size_--;
    }
}
void List::push_back(double value)
{
    if(size_+1==array_.size())
    {
        array_.resize();
    }
    array_.set(size_, value);
    size_++;
}
int List::index_of(double value)
{
    double eps=0.9;
    int i=-1;
    for (i=0; i< size_; i++)
    {
        if(array_.get(i)==value)
        {
            break;
        }
    }
    return i;
}
void List::remove(double value)
{
    int index=index_of(value);
    if (index=-1)
    {
        std::cerr << "error" << std::endl;
    }
    remove_at(index);
}
bool List::contains(double value)
{
    bool contain=false;
    for (size_t i=0; i<size_; i++)
    {
        if (array_.get(i)==value)
        {
            contain=true;
        }
    }
    return contain;
}
bool List::empty()
{
    return (size_==0);
}
void List::clear()
{
    size_=0;
}