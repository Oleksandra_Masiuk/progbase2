#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "dyn_list.h"

int openfile(List &l)
{
    int a=0;
    double number=0;
    FILE *fin = fopen("data.txt", "r");
    if (fin == NULL)
    {
        fprintf(stderr, "Error opening file \n");
        return EXIT_FAILURE;
    }
    while (fscanf(fin, "%lf", &number)>0)
    {
        l.push_back(number);
        a++;
    }
    if (fclose(fin) != 0)
    {
        fprintf(stderr, " Error closing file \n");
        return EXIT_FAILURE;
    }
    return a;
}

