#pragma once

#include <string>
#include <vector>
#include <fstream>

#include <optional>
#include "string_table.h"
#include "fs.h"
#include "list.h"
#include "dino.h"

using namespace std;

class FileStorage
{
   string  dir_name_;
   fstream dinosaurs_file_;
   vector<Dinosaur> loadDinosaurs();
   void saveDinosaurs(const vector<Dinosaur> & dinosaurs);
   int getNewDinosaurId();

 public:
   explicit FileStorage(const string & dir_name = "");
   void setName(const string & dir_name);
   string name() const;

   bool isOpen() const;
   bool open(); 
   void close();

   vector<Dinosaur> getAllDinosaurs(void);
   optional<Dinosaur> getDinosaurById(int dinosaur_id);
   bool updateDinosaur(const Dinosaur &dinosaur);
   bool removeDinosaur(int dinosaur_id);
   int insertDinosaur(const Dinosaur &dinosaur);
};
