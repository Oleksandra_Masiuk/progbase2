#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include "storage.h"
#include "cui.h"

int main()
{
    FileStorage storage{"./data/"};
    bool opt = storage.open();
    Cui cui{&storage};
    cui.show();
    storage.close();
}
