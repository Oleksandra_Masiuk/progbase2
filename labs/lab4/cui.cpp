#include "cui.h"
#include "storage.h"
using namespace std;
int ID = 0;
void Cui::dinosaursMainMenu()
{
    vector<Dinosaur> dino = storage_->getAllDinosaurs();
    for (Dinosaur &d : dino)
    {
        cout << d.id << " | " << d.name << endl;
    }
}
void Cui::dinosaurMenu(int dinosaur_id)
{
    optional<Dinosaur> dopt = storage_->getDinosaurById(dinosaur_id);
    if (dopt)
    {
        Dinosaur &d = dopt.value();
        cout << d.id << " | " << d.name << " | "
             << "w=" << d.weight << " | "
             << " l= " << d.length << " | "
             << "order: " << d.order << endl;
    }
    else
    {
        cout << "wrong id try again" << endl;
    }
}
void Cui::dinosaurUpdateMenu(int dinosaur_id)
{
    cout << "choose name, weight, length or order" << endl;
    string menu;
    cin >> menu;
    if (menu == "name" || menu == "weight" || menu == "length" || menu == "order")
    {
        optional<Dinosaur> dopt = storage_->getDinosaurById(dinosaur_id);
        if (dopt)
        {
            Dinosaur &d = dopt.value();
            if (menu == "name")
            {
                menu = "";
                cin >> menu;
                d.name = menu;
                if (storage_->updateDinosaur(d))
                {
                    cout << "dinosaur updated" << endl;
                }
                else
                {
                    cout << "cannot update this student" << endl;
                }
            }
            if (menu == "order")
            {
                menu = "";
                cin >> menu;
                d.order = menu;
                if (storage_->updateDinosaur(d))
                {
                    cout << "dinosaur updated" << endl;
                }
                else
                {
                    cout << "cannot update this student" << endl;
                }
            }
            if (menu == "weight")
            {
                char w[20];
                cin >> w;
                if ((w, "0") != 0 && atof(w) == 0)
                {
                    cout << "wrong weight" << endl;
                }
                else
                {
                    d.weight = atof(w);
                    if (storage_->updateDinosaur(d))
                    {
                        cout << "dinosaur updated" << endl;
                    }
                    else
                    {
                        cout << "cannot update this student" << endl;
                    }
                }
            }
            if (menu == "length")
            {
                char l[20];
                cin >> l;
                if ((l, "0") != 0 && atof(l) == 0)
                {
                    cout << "wrong weight" << endl;
                }
                else
                {
                    d.length = atof(l);
                    if (storage_->updateDinosaur(d))
                    {
                        cout << "dinosaur updated" << endl;
                    }
                    else
                    {
                        cout << "cannot update this student" << endl;
                    }
                }
            }
        }
        else
        {
            cout << "wrong id, try again" << endl;
        }
    }
    else
    {
        cout << "wrong option try again";
    }
}
void Cui::dinosaurDeleteMenu(int dinosaur_id)
{
    bool status = storage_->removeDinosaur(dinosaur_id);
    if (status)
    {
        cout << "removed" << endl;
    }
    else
    {
        cout << "cannot remove" << endl;
    }
}
void Cui::dinosaurCreateMenu()
{
    string name;
    char weight[20];
    string order;
    char length[20];
    cout << "please, enter name, weight, length and order" << endl;
    cin >> name;
    cin >> weight;
    cin >> length;
    cin >> order;
    if ((atof(weight) == 0 && (weight, "0") != 0) || (atof(length) == 0 && (length, "0") != 0))
    {
        cout << "wrong length or weigth " << endl;
    }
    else
    {
        Dinosaur d{name, stod(weight), stod(length), order, -1};
        int newId = storage_->insertDinosaur(d);
        d.id = newId;
    }
}
void Cui::show()
{
    string menu;
    while (menu != "exit")
    {
        cout << "enter:" << endl
             << "get_dino or get_all_dino or update_dino or remove_dino or add_dino or exit" << endl;
        cin >> menu;
        if (menu != "exit" && menu != "get_dino" && menu != "get_all_dino" && menu != "remove_dino" && menu != "update_dino" && menu != "add_dino")
        {
            cout << "wrong option, try again" << endl;
        }
        else if (menu == "get_dino")
        {
            char id[20];
            cout << "enter id" << endl;
            cin >> id;
            if ((atof(id) == 0 && (id, "0") != 0))
            {
                cout << "wrong id" << endl;
            }
            else
            {
                this->dinosaurMenu(atoi(id));
            }
        }
        else if (menu == "get_all_dino")
        {
            this->dinosaursMainMenu();
        }
        else if (menu == "update_dino")
        {
            char id[20];
            cout << "enter id" << endl;
            cin >> id;
            if ((atoi(id) == 0 && (id, "0") != 0))
            {
                cout << "wrong id" << endl;
            }
            else
            {
                this->dinosaurUpdateMenu(atoi(id));
            }
        }
        else if (menu == "remove_dino")
        {
            char id[20];
            cout << "enter id" << endl;
            cin >> id;
            if ((atoi(id) == 0 && (id, "0") != 0))
            {
                cout << "wrong id" << endl;
            }
            else
            {
                this->dinosaurDeleteMenu(atoi(id));
            }
        }
        else if (menu == "add_dino")
        {
            this->dinosaurCreateMenu();
        }
    }
}