using namespace std;
#include <iostream>
#include "storage.h"
#include "dino.h"
#include <optional>
#include <vector>
#pragma once
class Cui
{
    FileStorage * const storage_;

    // students menus
    void dinosaursMainMenu();
    void dinosaurMenu(int dinosaur_id);
    void dinosaurUpdateMenu(int dinosaur_id);
    void dinosaurDeleteMenu(int dinosaur_id);
    void dinosaurCreateMenu();
    
public:
    Cui(FileStorage * storage): storage_{storage} {}
    //
    void show();
};
