#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include "string_table.h"
#include "dino.h"
#include <optional>
#include <vector>
using namespace std;
#pragma once


vector<Dinosaur> createDinoListFromTable(StringTable &csvTable);
StringTable createTableFromList(vector<Dinosaur> v, size_t cols);