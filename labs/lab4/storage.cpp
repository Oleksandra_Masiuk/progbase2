#include "storage.h"
using namespace std;

FileStorage::FileStorage(const string &dir_name)
{
    dir_name_ = dir_name;
}
void FileStorage::setName(const string &dir_name)
{
    dir_name_ = dir_name;
}
string FileStorage::name() const
{
    return dir_name_;
}

bool FileStorage::isOpen() const
{
    return dinosaurs_file_.is_open();
}
bool FileStorage::open()
{
    dinosaurs_file_.open(dir_name_ + "dinosaur.csv");
    return dinosaurs_file_.is_open();
}
void FileStorage::close()
{
    dinosaurs_file_.close();
}

vector<Dinosaur> FileStorage::getAllDinosaurs(void)
{
    return this->loadDinosaurs();
}
optional<Dinosaur> FileStorage::getDinosaurById(int dinosaur_id)
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id == dinosaur_id)
        {
            return dino;
        }
    }
    return nullopt;
}
bool FileStorage::updateDinosaur(const Dinosaur &dinosaur)
{
    int dinosaur_id = dinosaur.id;
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id == dinosaur_id)
        {
            dino = dinosaur;
            this->saveDinosaurs(dinosaurs);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeDinosaur(int dinosaur_id)
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    auto it = dinosaurs.begin();
    auto it_end = dinosaurs.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == dinosaur_id)
        {
            dinosaurs.erase(it);
            this->saveDinosaurs(dinosaurs);
            return true;
        }
    }
    return false;
}
int FileStorage::insertDinosaur(const Dinosaur &dinosaur)
{
    int newId = this->getNewDinosaurId();
    Dinosaur copy = dinosaur;
    copy.id = newId;
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    dinosaurs.push_back(copy);
    this->saveDinosaurs(dinosaurs);
    return newId;
}

vector<Dinosaur> FileStorage::loadDinosaurs()
{
    dinosaurs_file_.close();
    dinosaurs_file_.open(dir_name_ + "dinosaur.csv", ios::in);
    StringTable table = readfile(dinosaurs_file_);
    vector<Dinosaur> dinosaurs = createDinoListFromTable(table);
    return dinosaurs;
}
void FileStorage::saveDinosaurs(const vector<Dinosaur> &dinosaurs)
{
    dinosaurs_file_.close();
    dinosaurs_file_.open(dir_name_ + "dinosaur.csv", ios::out);
    StringTable table = createTableFromList(dinosaurs, 5);
    string csvtable = Csv_toString(table);
    writefile(dinosaurs_file_, csvtable);
}
int FileStorage::getNewDinosaurId()
{
    vector<Dinosaur> dinosaurs = this->loadDinosaurs();
    int max = 0;
    for (Dinosaur &dino : dinosaurs)
    {
        if (dino.id > max)
        {
            max = dino.id;
        }
    }
    return max + 1;
}