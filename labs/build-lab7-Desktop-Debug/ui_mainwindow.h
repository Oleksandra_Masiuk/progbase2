/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionNew_storage;
    QAction *actionOpen_storage;
    QWidget *centralWidget;
    QListWidget *listWidget;
    QWidget *widget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_id;
    QLabel *label_name;
    QLabel *label_weight;
    QLabel *label_length;
    QLabel *label_order;
    QLabel *label_6;
    QLabel *dino;
    QPushButton *Add;
    QPushButton *Edit;
    QPushButton *Remove;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(341, 492);
        MainWindow->setStyleSheet(QStringLiteral("background-color:rgb(73, 170, 168)"));
        actionNew_storage = new QAction(MainWindow);
        actionNew_storage->setObjectName(QStringLiteral("actionNew_storage"));
        actionOpen_storage = new QAction(MainWindow);
        actionOpen_storage->setObjectName(QStringLiteral("actionOpen_storage"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 70, 231, 121));
        listWidget->setStyleSheet(QStringLiteral("background-color:rgb(168, 224, 234)"));
        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 250, 321, 191));
        horizontalLayoutWidget = new QWidget(widget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(-1, -1, 271, 181));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        label_2 = new QLabel(horizontalLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_2);

        label_3 = new QLabel(horizontalLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(horizontalLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_4);

        label_5 = new QLabel(horizontalLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label_5);


        horizontalLayout->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_id = new QLabel(horizontalLayoutWidget);
        label_id->setObjectName(QStringLiteral("label_id"));

        verticalLayout_2->addWidget(label_id);

        label_name = new QLabel(horizontalLayoutWidget);
        label_name->setObjectName(QStringLiteral("label_name"));

        verticalLayout_2->addWidget(label_name);

        label_weight = new QLabel(horizontalLayoutWidget);
        label_weight->setObjectName(QStringLiteral("label_weight"));

        verticalLayout_2->addWidget(label_weight);

        label_length = new QLabel(horizontalLayoutWidget);
        label_length->setObjectName(QStringLiteral("label_length"));

        verticalLayout_2->addWidget(label_length);

        label_order = new QLabel(horizontalLayoutWidget);
        label_order->setObjectName(QStringLiteral("label_order"));

        verticalLayout_2->addWidget(label_order);


        horizontalLayout->addLayout(verticalLayout_2);

        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 220, 141, 16));
        label_6->setStyleSheet(QStringLiteral("font: 59 italic 11pt \"Ubuntu\";"));
        dino = new QLabel(centralWidget);
        dino->setObjectName(QStringLiteral("dino"));
        dino->setGeometry(QRect(20, 40, 111, 21));
        Add = new QPushButton(centralWidget);
        Add->setObjectName(QStringLiteral("Add"));
        Add->setEnabled(false);
        Add->setGeometry(QRect(10, 10, 89, 25));
        Edit = new QPushButton(centralWidget);
        Edit->setObjectName(QStringLiteral("Edit"));
        Edit->setEnabled(false);
        Edit->setGeometry(QRect(110, 10, 89, 25));
        Remove = new QPushButton(centralWidget);
        Remove->setObjectName(QStringLiteral("Remove"));
        Remove->setEnabled(false);
        Remove->setGeometry(QRect(210, 10, 89, 25));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 341, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionNew_storage);
        menuFile->addAction(actionOpen_storage);
        menuFile->addSeparator();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Dinosaur database", Q_NULLPTR));
        actionNew_storage->setText(QApplication::translate("MainWindow", "New storage...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionNew_storage->setShortcut(QApplication::translate("MainWindow", "Ctrl+N", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        actionOpen_storage->setText(QApplication::translate("MainWindow", "Open storage...", Q_NULLPTR));
#ifndef QT_NO_SHORTCUT
        actionOpen_storage->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", Q_NULLPTR));
#endif // QT_NO_SHORTCUT
        label->setText(QApplication::translate("MainWindow", "id", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "name", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindow", "weight", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "length", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "order", Q_NULLPTR));
        label_id->setText(QString());
        label_name->setText(QString());
        label_weight->setText(QString());
        label_length->setText(QString());
        label_order->setText(QString());
        label_6->setText(QApplication::translate("MainWindow", "Selected Dinosaur:", Q_NULLPTR));
        dino->setText(QApplication::translate("MainWindow", "Dinosaur's list:", Q_NULLPTR));
        Add->setText(QApplication::translate("MainWindow", "Add", Q_NULLPTR));
        Edit->setText(QApplication::translate("MainWindow", "Edit", Q_NULLPTR));
        Remove->setText(QApplication::translate("MainWindow", "Remove", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
